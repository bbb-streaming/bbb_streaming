# BBB Streaming


Ziel ist es, BBB-Konferenzen per WebRTC zu streamen.

## Architektur

* Chrome läuft in Xvfb
* gstreamer greift Bild und Audio ab und erzeugt RTP Video Streams in verschiedenen Auflösungen und Audio
* Janus mit Streaming Plugin verteilt diese Daten
* Chrome und gstreamer laufen zusammen (z.B. als Container), pro BBB-Raum eine Instanz

~~~mermaid
graph LR
    A[Chrome]
    B(Gstreamer)
    C{Janus + Streaming Plugin}
    D[BBB]
    E[Safari]
    F[Firefox]
    G[Chrome]

    A --> |nimmt teil| D
    subgraph Worker
    A -->|Xvfb\n PulseAudio| B
    end
    B --> |RTP Streams| C
    C -->|WebRTC| E
    C -->|WebRTC| F
    C -->|WebRTC| G
~~~

## Zeitlicher Ablauf

* BBB-Raum startet
* Raumeigentümer startet Streaming
* im Janus wird per API ein Streamingraum eingerichtet, RTP Ports für Empfang werden dabei festgelegt
* Worker aus Chrome und GStreamer fängt an zu arbeiten, sendet Daten an Janus
* Raumeigentümer beendet Streaming
* Worker aus Chrome und GStreamer wird beendet
* Streamingraum im Janus wird gelöscht
* BBB-Raum wird beendet

## Ideen

* es gibt N Woker, die permanent aktiv sind
* Es gibt eine Streamingsteuerung, die in der Datenbank vom Greenlight lesen darf.
* Die Streamingsteuerung stellt auch die Benutzerschnittstelle für Streamzuschauer bereit.
* URL des Streams leitet sich aus URL des Raums ab. Dadurch kann man vor Streamingbeginn auf die Seite verweisen.
* Die Kommunikation zwischen Streamingsteuerung und Worker wird über eine Message-Queue durchgeführt (z.B. RabbitMQ)
* Worker warten auf Aufträge an Message-Queue. Message-Queue garantiert, dass nur ein Worker einen Auftrag bekommt.
* Pro Worker gibt es eine Message-Queue, in der Nachrichten von der Streaminsteuerung zum Worker geschickt werden.
* Streamingsteuerung prüft periodisch ob BBB Raum noch aktiv ist und beendet ggf. das Streaming.

~~~mermaid
sequenceDiagram
    participant S as Streamingsteuerung
    participant Q_all as Alle Worker
    participant Q_j as Janus
    participant Q_w as Worker Y
    S ->> Q_j: Erstelle Streaming-Endpunkt
    S ->> Q_all: Starte Streaming Raum X (inkl. Parameter)
    Q_w ->> S: Streaming Raum X, Nutze Queue `to_worker_y`
    S ->> Q_w: Ändere BBB-Einstellungen/Ansicht (Chat, Präsentation, Sprecher)
    S ->> Q_w: Stoppe Streaming
~~~


### Streaming Worker

#### States

~~~mermaid
stateDiagram-v2
    [*] --> Idle: Start Worker
  
    Idle --> BBBInactive_PlayIdle: [new-job]
    BBBInactive_PlayIdle --> BBBActive_PlayIdle: [room-active]
    BBBActive_PlayIdle --> BBBActive_Streaming: [unpause]
    BBBActive_Streaming --> BBBActive_PlayIdle: [pause]

    BBBActive_PlayIdle --> BBBInactive_PlayIdle: [room-terminated]
    BBBActive_Streaming --> BBBInactive_Streaming: [room-terminated]
    BBBInactive_Streaming --> BBBActive_Streaming: [room-active]

    BBBInactive_Streaming --> BBBInactive_PlayIdle: [pause]

    BBBInactive_PlayIdle --> Idle: [stop-streaming]
    BBBActive_PlayIdle --> Idle: [stop-streaming]
    BBBActive_Streaming --> Idle: [stop-streaming]
    BBBInactive_Streaming --> Idle: [stop-streaming]

    Idle --> Stopped: [stop-worker]
    BBBInactive_PlayIdle --> Stopped: [stop-worker]
    BBBActive_PlayIdle --> Stopped: [stop-worker]
    BBBActive_Streaming --> Stopped: [stop-worker]

    Stopped --> [*]
~~~

#### Activity Overview

~~~mermaid
graph LR
    A((Worker Started))
    B(Idle)
    C(Streaming)
    D(Waiting)
    E{Room active?}
    F((Worker Stopped))
    G(Register Worker)
    H{Retry Room?}
    I(Unregister Worker)

    A --> G
    G --> B
    B -- Start Streaming --> E
    E -- YES --> C
    E -- NO --> H
    D --> E
    H -- YES --> D
    H -- NO --> B
    C -- Stop Streaming --> B
    B -- Stop Worker --> I
    C -- Stop Worker --> I
    D -- Stop Worker --> I
    F --> I
~~~


### Django Einstellungen

```python
# RabbitMQ Connection Params
RABBIT_CONNECTION = {
    'host': '192.168.121.111'
}

RABBIT_REVEIVE_QUEUE = 'd20cf59d-b1ca-4de3-97d5-0450930b12f0'
RABBIT_WORKER_QUEUE = '1f4b1cce-8ed0-4374-99db-076448652e71'

JANUS_API_ENDPOINT = 'http://192.168.121.101:8088/janus'
JANUS_API_STREAMING_ADMIN_KEY = 'supersecret'
JANUS_MEDIA_IP = '192.168.121.101'
JANUS_MEDIA_PORT_START = 5000
JANUS_MEDIA_PORT_STEP = 10

# Join URL will be `<BBB_JOIN_URL>/<greenlight-id>`
BBB_JOIN_URL = "http://<bbb-server>/b"

# BBB-API Endpunkt
BBB_API_ENDPOINT = 'http://<bbb-server>/bigbluebutton/api/'
BBB_API_SECRET = 'CHANGEME'

# Filter Social Domain (search exact <username>@<social_domain>)
GREENLIGHT_SOCAL_DOMAIN = 'example.com'
```
