import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="bbb_streaming",
    version="0.0.1",
    author="Tobias Gall, Daniel Schreiber",
    author_email="tobias.gall@mailbox.org, schrd@linux-tage.de",
    description="A management tool for streaming BigBlueButton sessions",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(include=["bbb_streaming", "bbb_streaming.management.commands"]),
    package_data={
        'bbb_streaming': [
            'templates/base/*',
            'templates/bbb_streaming/*',
            'static/*',
            'static/img/*',
        ],
    },
    install_requires=[
        "Django>=3.1,<3.2",
        "pika",
        "requests",
        "bigbluebutton_api_python",
        "django-crispy-forms",
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
