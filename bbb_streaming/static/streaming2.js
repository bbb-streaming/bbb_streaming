let server = null;
let janus = null;
let streaming = null;
const opaqueId = "streaming-" + Janus.randomString(12);

let bitrateTimer = null;
let simulcastStarted = false;
let resolutionBadge = null;
let bitrateBadge = null;
let viewersBadge = null;
let mainVideo = null;
let playButton = null;
let videoWrapper = null;

let selectedStream = null;

$(document).ready(function () {
    mainVideo = $('#main-video');
    resolutionBadge = document.querySelector('#resolution');
    bitrateBadge = document.querySelector('#bitrate');
    viewersBadge = document.querySelector('#viewers');

    playButton = $('#play-btn');

    selectedStream = mainVideo.data("janus");
    server = mainVideo.data('janus-api');

    videoWrapper = document.querySelector('#video-wrapper');
    toastr.options.showMethod = "show";
    toastr.options.hideMethod = "hide";
    toastr.options.closeMethod = "hide";

    // Initialize the library (all console debuggers enabled)
    Janus.init({
        debug: "all",
        callback: function () {
            janus = new Janus({
                server: server,
                iceServers: [
                    {
                        urls: videoWrapper.dataset.stunuri,
                    },
                    {
                        urls: videoWrapper.dataset.turnuri,
                        username: videoWrapper.dataset.turnuser,
                        credential: videoWrapper.dataset.turnsecret,
                    }
                ],
                success: function () {
                    // Attach to Streaming plugin
                    janus.attach({
                        plugin: "janus.plugin.streaming",
                        opaqueId: opaqueId,
                        success: function (pluginHandle) {
                            streaming = pluginHandle;
                            Janus.log("Plugin attached! (" + streaming.getPlugin() + ", id=" + streaming.getId() + ")");
                        },
                        error: function (error) {
                            Janus.error("Error attaching plugin... ", error);
                            bootbox.alert("Error attaching plugin... " + error);
                        },
                        iceState: function (state) {
                            Janus.log("ICE state changed to " + state);
                        },
                        webrtcState: function (on) {
                            Janus.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
                        },
                        onmessage: function (msg, jsep) {
                            Janus.debug("Got a message ", msg);
                            const result = msg["result"];
                            if (result) {
                                if (result["status"]) {
                                    const status = result["status"];
                                    if (status === 'starting') {
                                    } else if (status === 'started') {
                                    } else if (status === 'stopped') {
                                        stopStream()
                                    }
                                } else if (msg["streaming"] === "event") {
                                    // Is simulcast in place?
                                    const substream = result["substream"];
                                    let temporal = result["temporal"];
                                    if ((substream !== null && substream !== undefined) || (temporal !== null && temporal !== undefined)) {
                                        if (!simulcastStarted) {
                                            simulcastStarted = true;
                                            addSimulcastButtons(temporal !== null && temporal !== undefined);
                                        }
                                        // We just received notice that there's been a switch, update the buttons
                                        updateSimulcastButtons(substream, temporal);
                                    }
                                }
                            } else if (msg["error"]) {
                                bootbox.alert(msg["error"]);
                                stopStream();
                                return;
                            }
                            if (jsep) {
                                Janus.debug("Handling SDP as well...", jsep);
                                let stereo = (jsep.sdp.indexOf("stereo=1") !== -1);
                                // Offer from the plugin, let's answer
                                streaming.createAnswer(
                                    {
                                        jsep: jsep,
                                        // We want recvonly audio/video and, if negotiated, datachannels
                                        media: {audioSend: false, videoSend: false, data: true},
                                        customizeSdp: function (jsep) {
                                            if (stereo && jsep.sdp.indexOf("stereo=1") == -1) {
                                                // Make sure that our offer contains stereo too
                                                jsep.sdp = jsep.sdp.replace("useinbandfec=1", "useinbandfec=1;stereo=1");
                                            }
                                        },
                                        success: function (jsep) {
                                            Janus.debug("Got SDP!", jsep);
                                            var body = {request: "start"};
                                            streaming.send({message: body, jsep: jsep});
                                        },
                                        error: function (error) {
                                            Janus.error("WebRTC error:", error);
                                            bootbox.alert("WebRTC error... " + error.message);
                                        }
                                    });
                            }
                        },
                        onremotestream: function (stream) {
                            Janus.debug("Got a remote stream", stream);
                            mainVideo.get(0).volume = 0;
                            // Show the stream
                            mainVideo.bind("playing", function () {
                                const videoTracks = stream.getVideoTracks();
                                if (!videoTracks || videoTracks.length === 0)
                                    return;
                                const width = this.videoWidth;
                                const height = this.videoHeight;
                                resolutionBadge.innerText = `${width}x${height}`;

                                if (Janus.webRTCAdapter.browserDetails.browser === "firefox") {
                                    // Firefox Stable has a bug: width and height are not immediately available after a playing
                                    setTimeout(function () {
                                        const width = mainVideo.get(0).videoWidth;
                                        const height = mainVideo.get(0).videoHeight;
                                        resolutionBadge.innerText = `${width}x${height}`;
                                    }, 2000);
                                }
                            });

                            Janus.attachMediaStream(mainVideo.get(0), stream);
                            mainVideo.get(0).play();
                            mainVideo.get(0).volume = 1;
                            const videoTracks = stream.getVideoTracks();
                            if (!videoTracks || videoTracks.length === 0) {
                                // No remote video
                                bootbox.alert("No Remote Video available")
                            }
                            if (videoTracks && videoTracks.length &&
                                (Janus.webRTCAdapter.browserDetails.browser === "chrome" ||
                                    Janus.webRTCAdapter.browserDetails.browser === "firefox" ||
                                    Janus.webRTCAdapter.browserDetails.browser === "safari")) {
                                bitrateTimer = setInterval(function () {
                                    // Display updated bitrate, if supported
                                    const bitrate = streaming.getBitrate();
                                    bitrateBadge.innerText = `${bitrate}`;
                                    const width = mainVideo.get(0).videoWidth;
									const height = mainVideo.get(0).videoHeight;
									if(width > 0 && height > 0) {
                                        resolutionBadge.innerText = `${width}x${height}`;
                                    }
                                }, 1000);
                            }
                        },
                        ondataopen: function (data) {
                            Janus.log("The DataChannel is available!");
                        },
                        ondata: function (data) {
                            receivedData(data);
                            Janus.debug("We got data from the DataChannel!", data);
                        },
                        oncleanup: function () {
                            Janus.log("Got a cleanup notification");
                            if (bitrateTimer)
                                clearInterval(bitrateTimer);
                            bitrateTimer = null;
                            simulcastStarted = false;
                            resolutionBadge.innerText = '';
                            bitrateBadge.innerText = '';
                        }
                    });
                },
                error: function (error) {
                    Janus.error(error);
                    bootbox.alert(error, function () {
                        window.location.reload();
                    });
                },
                destroyed: function () {
                    window.location.reload();
                }
            });
        }
    });

    playButton.click(function () {
        startStream();
        playButton.hide();
        mainVideo.prop("controls", true);
    });

});

function receivedData(byte_data) {
    const data = JSON.parse(byte_data);
    if (data.hasOwnProperty('viewers')) {
        const viewers_string = data['viewers'] !== 1 ? 'Viewers' : 'Viewer';
        viewersBadge.innerText = `${data['viewers']} ${viewers_string}`;
    }
}

function getStreamInfo() {
    if (!selectedStream)
        return;
    // Send a request for more info on the mountpoint we subscribed to
    const body = {request: "info", id: selectedStream};
    streaming.send({
        message: body, success: function (result) {
            console.log(result);
        }
    });
}

function startStream() {
    Janus.log("Selected video id #" + selectedStream);
    if (!selectedStream) {
        bootbox.alert("No stream selected");
        return;
    }

    const body = {request: "watch", id: selectedStream};
    streaming.send({message: body});
    // Get some more info for the mountpoint to display, if any
    //getStreamInfo();
}

function stopStream() {
    const body = {request: "stop"};
    streaming.send({message: body});
    streaming.hangup();
    if (bitrateTimer)
        clearInterval(bitrateTimer);
    bitrateTimer = null;
    simulcastStarted = false;
    resolutionBadge.innerText = '';
    bitrateBadge.innerText = '';
    playButton.show();
    mainVideo.prop("controls", false);
}

// Helpers to create Simulcast-related UI, if enabled
function addSimulcastButtons(temporal) {
    const simulcast_buttons = document.querySelector('#simulcast_buttons');
    simulcast_buttons.innerHTML = `
        <div class="btn-group btn-group-xs">
            <button id="sl-0" type="button" class="btn btn-info" data-toggle="tooltip" title="Switch to lower quality">540p</button>
            <button id="sl-1" type="button" class="btn btn-info" data-toggle="tooltip" title="Switch to normal quality">720p</button>
            <button id="sl-2" type="button" class="btn btn-info" data-toggle="tooltip" title="Switch to higher quality">1080p</button>
        </div>`
    // Enable the simulcast selection buttons
    const sl_0 = simulcast_buttons.querySelector('#sl-0');
    const sl_1 = simulcast_buttons.querySelector('#sl-1');
    const sl_2 = simulcast_buttons.querySelector('#sl-2');

    $(sl_0).attr('class', 'btn btn-info').unbind('click').click(function () {
        toastr.info("Switching simulcast substream, wait for it... (lower quality)", null, {timeOut: 2000});

        $(sl_2).attr('class', 'btn btn-info');
        $(sl_1).attr('class', 'btn btn-info');
        $(sl_0).attr('class', 'btn btn-primary');
        streaming.send({message: {request: "configure", substream: 0}});
    });

    $(sl_1).attr('class', 'btn btn-info').unbind('click').click(function () {
        toastr.info("Switching simulcast substream, wait for it... (normal quality)", null, {timeOut: 2000});

        $(sl_2).attr('class', 'btn btn-info');
        $(sl_1).attr('class', 'btn btn-primary');
        $(sl_0).attr('class', 'btn btn-info');
        streaming.send({message: {request: "configure", substream: 1}});
    });

    $(sl_2).attr('class', 'btn btn-info').unbind('click').click(function () {
        toastr.info("Switching simulcast substream, wait for it... (higher quality)", null, {timeOut: 2000});

        $(sl_2).attr('class', 'btn btn-primary');
        $(sl_1).attr('class', 'btn btn-info');
        $(sl_0).attr('class', 'btn btn-info');
        streaming.send({message: {request: "configure", substream: 2}});
    });

}

function updateSimulcastButtons(substream, temporal) {
    const simulcast_buttons = document.querySelector('#simulcast_buttons');

    const sl_0 = simulcast_buttons.querySelector('#sl-0');
    const sl_1 = simulcast_buttons.querySelector('#sl-1');
    const sl_2 = simulcast_buttons.querySelector('#sl-2');

    // Check the substream
    if (substream === 0) {
        toastr.success("Switched simulcast substream! (lower quality)", null, {timeOut: 2000});

        $(sl_2).attr('class', 'btn btn-info');
        $(sl_1).attr('class', 'btn btn-info');
        $(sl_0).attr('class', 'btn btn-primary');

    } else if (substream === 1) {
        toastr.success("Switched simulcast substream! (normal quality)", null, {timeOut: 2000});

        $(sl_2).attr('class', 'btn btn-info');
        $(sl_1).attr('class', 'btn btn-primary');
        $(sl_0).attr('class', 'btn btn-info');

    } else if (substream === 2) {
        toastr.success("Switched simulcast substream! (higher quality)", null, {timeOut: 2000});

        $(sl_2).attr('class', 'btn btn-primary');
        $(sl_1).attr('class', 'btn btn-info');
        $(sl_0).attr('class', 'btn btn-info');

    }

}
