function stream_action(gl_id, url) {
    const data = new FormData();
    const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;

    data.append('external_id', gl_id);

    const request = new Request(
        url,
        {headers: {'X-CSRFToken': csrftoken}}
    );

    fetch(request, {
        method: 'POST',
        mode: 'same-origin',
        body: data,
    })
        .then(result => { window.location = result.url })
        .catch(console.error);
}

function copyLink(el) {
    const link = el.parentElement.querySelector('.watch-link').href;
    navigator.clipboard.writeText(link).then(function () {
            toastr.success("Link copied to clipboard!", null, {timeOut: 2000});
            console.log('Link copied to clipboard.');
        }, function () {
            console.log('Link not copied to clipboard.');
        }
    );
}

$(document).ready(function() {
    document.querySelectorAll('.room_action').forEach(item => {
        item.addEventListener('click', e => {
            console.log(e.target)
            console.log(e.target.dataset.glid);
            stream_action(e.target.dataset.glid, e.target.dataset.url);
        })
    })

    document.querySelectorAll('.copy_link').forEach(item => {
        item.addEventListener('click', e => {
            copyLink(e.target);
        })
    })
});
