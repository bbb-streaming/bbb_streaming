import base64
from hashlib import sha1
import hmac
import logging
import random
import string
from datetime import timedelta


from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, Http404
from django.shortcuts import get_object_or_404, render, redirect
from django.utils import timezone
from django.utils.module_loading import import_string
from django.utils.translation import gettext_lazy as _

bbb_provider = import_string(settings.BBB_PROVIDER)

from bbb_streaming.models import Room, Worker
from .forms import RoomForm

logger = logging.getLogger(__name__)


def random_string_generator(str_size, allowed_chars):
    return ''.join(random.choice(allowed_chars) for x in range(str_size))


def watch_room(request, id):
    room = get_object_or_404(Room, id=id)
    timestamp = (timezone.now() + timedelta(days=1)).timestamp()
    randomname = random_string_generator(12, string.ascii_letters)
    turnuser = '{}:{}'.format(int(timestamp), randomname)
    turnsecret = hmac.new(bytes(settings.TURN_SECRET, 'utf-8'), bytes(turnuser, 'utf-8'), sha1)

    janus_api = None
    if hasattr(settings, 'JANUS_LB_WSS_URL'):
        janus_api = settings.JANUS_LB_WSS_URL
    elif hasattr(settings, 'JANUS_LB_REST_URL'):
        janus_api = settings.JANUS_LB_REST_URL
    else:
        try:
            random_endpoint = random.choice(room.streaming_endpoints.filter(enabled=True))
        except IndexError:
            pass
        else:
            if random_endpoint.janus_server.ws_endpoint:
                janus_api = random_endpoint.janus_server.ws_endpoint
            else:
                janus_api = random_endpoint.janus_server.rest_endpoint

    return render(request, 'bbb_streaming/watch.html', {
        'room': room,
        'janus_api': janus_api,
        'base_template': getattr(settings, 'THEME_BASE_WATCH', 'base/bootstrap.html'),
        'turnuser': turnuser,
        'turnsecret': str(base64.b64encode(turnsecret.digest()), 'utf-8'),
        'turnuri': settings.TURN_URI,
        'stunuri': settings.STUN_URI,
    })


def room_viewers(request, id):
    room = get_object_or_404(Room, id=id)
    return JsonResponse({'viewers': room.get_viewers()})


@login_required
def list_rooms(request):
    external_rooms = bbb_provider.get_user_rooms(request.user)
    rooms = Room.objects.filter(external_id__in=[x['uid'] for x in external_rooms])

    room_list = []

    for er in external_rooms:
        try:
            r = rooms.get(external_id=er['uid'])
        except Room.DoesNotExist:
            r = None

        room_list.append({
            'er': er,
            'room': r,
            'join_url': bbb_provider.get_room_url(er['uid']) ,
        })

    return render(request, 'bbb_streaming/list.html', {
        'rooms': room_list,
        'STATES': Worker.State.__members__,
        'base_template': getattr(settings, 'THEME_BASE_LIST', 'base/bootstrap.html'),
    })


def _room_action(request, action):
    # Todo: Check if room belong to user
    if request.method == "POST":
        external_id = request.POST.get('external_id', None)
        if external_id is None:
            messages.add_message(request, messages.WARNING, _('Requested Room not found'))
        else:
            if action == Room.Action.START:
                room, _created = Room.objects.get_or_create(external_id=external_id)
            else:
                room = get_object_or_404(Room, external_id=external_id)
            room.set_action(action)

    return redirect('list_rooms')


@login_required
def start_room(request):
    return _room_action(request, Room.Action.START)


@login_required
def stop_room(request):
    return _room_action(request, Room.Action.STOP)


@login_required
def pause_room(request):
    return _room_action(request, Room.Action.PAUSE)


@login_required
def unpause_room(request):
    return _room_action(request, Room.Action.UNPAUSE)

@login_required
def edit_room(request, external_id):
    room, _created = Room.objects.get_or_create(external_id=external_id)
    if request.method == 'POST':
        form = RoomForm(request.POST, instance=room)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.INFO, _('Room updated'))
            return redirect('list_rooms')
    else:
        form = RoomForm(instance=room)
    return render(request, 'bbb_streaming/edit_room.html', {
        'room': room,
        'form': form,
        'base_template': getattr(settings, 'THEME_BASE_LIST', 'base/bootstrap.html'),
    })

