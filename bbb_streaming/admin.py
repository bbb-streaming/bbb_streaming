from django.contrib import admin

from bbb_streaming.models import *
from django.utils.translation import gettext_lazy as _


class StreamingEndpointExceptionLogInline(admin.TabularInline):
    model = StreamingEndpointExceptionLog
    readonly_fields = ['time', 'log', 'response']
    extra = 0
    can_delete = False

    def has_add_permission(self, request, obj=None):
        return False


class RoomExceptionLogInline(admin.TabularInline):
    model = RoomExceptionLog
    readonly_fields = ['time', 'log']
    extra = 0
    can_delete = False

    def has_add_permission(self, request, obj=None):
        return False


@admin.register(StreamingEndpoint)
class StreamingEndpointAdmin(admin.ModelAdmin):
    list_display_links = ['id']
    list_display = ('id', 'enabled', 'created', 'recording', 'janus_server', 'get_room')
    readonly_fields = ['recording']

    list_filter = (
        ('enabled', admin.BooleanFieldListFilter),
        ('created', admin.BooleanFieldListFilter),
        ('recording', admin.BooleanFieldListFilter),
    )

    search_fields = (
        'room__external_id',
    )

    actions = [
        'action_redeploy',
    ]

    inlines = [
        StreamingEndpointExceptionLogInline,
    ]

    def get_room(self, obj):
        return ", ".join([str(r) for r in obj.room_set.all()])
    get_room.short_description = _("Room")

    def action_redeploy(self, request, queryset):
        for se in queryset:
            se.deploy()
    action_redeploy.short_description = _("Redeploy endpoint")


@admin.register(JanusStreamingServer)
class JanusStreamingServerAdmin(admin.ModelAdmin):
    list_display = ('rest_endpoint', 'rtp_ip', 'enabled')

@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    list_display = ['external_id', 'id', 'action', 'worker']
    list_display_links = ['external_id', 'id']
    search_fields = ['external_id']

    list_filter = (
        ('action', admin.ChoicesFieldListFilter),
    )

    inlines = [
        RoomExceptionLogInline,
    ]


@admin.register(Worker)
class WorkerAdmin(admin.ModelAdmin):
    list_display = ['id', 'state', 'room']
    list_display_links = ['id', 'state']

    list_filter = (
        ('state', admin.ChoicesFieldListFilter),
    )
