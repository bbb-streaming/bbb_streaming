from django.apps import AppConfig


class BBBStreamingConfig(AppConfig):
    name = 'bbb_streaming'

    def ready(self):
        import bbb_streaming.signals  # noqa
