from django.urls import path

from .views import *

urlpatterns = [
    path('watch/<uuid:id>/', watch_room, name='watch_room'),
    path('list/', list_rooms, name='list_rooms'),
    path('start/', start_room, name='start_room'),
    path('stop/', stop_room, name='stop_room'),
    path('pause/', pause_room, name='pause_room'),
    path('unpause/', unpause_room, name='unpause_room'),
    path('fetch/viewers/<uuid:id>/', room_viewers, name='room_viewers'),
    path('edit/<str:external_id>/', edit_room, name='edit_room'),
]
