from abc import ABC, abstractmethod


class BBBProviderInterface(ABC):

    @classmethod
    @abstractmethod
    def get_user_rooms(cls, user):
        """
        Return unique list of Rooms for given user

        :param user: User model instance
        :type user: User

        :return: List of Dicts like [{'uid': '', 'name': ''}]
        :rtype: list
        """
        pass

    @classmethod
    @abstractmethod
    def get_meeting_running_url(cls, external_id):
        """
        Returns a URL to check if a meeting is running

        :param external_id: Room model external_id
        :type external_id: string

        :return: URL
        :rtype: string
        """
        pass

    @classmethod
    @abstractmethod
    def is_meeting_running(cls, external_id):
        """
        Check if a meeting is currently running.
        This URL will be sent to the worker.

        :param external_id: Room model external_id
        :type external_id: string

        :return:
        :rtype: bool
        """
        pass

    @classmethod
    @abstractmethod
    def get_meeting_join_url(cls, external_id, additional_urlparams=None):
        """
        Get an URL to directly join a BBB Meeting.
        This URL will be sent to the worker.

        :param external_id: Room model external_id
        :type external_id: string

        :param additional_urlparams: Additional URL Parameter
        :type additional_urlparams: dict

        :return: URL
        :rtype: string
        """
        pass

    @classmethod
    @abstractmethod
    def get_room_url(cls, external_id):
        """
        Get an URL to join a BBB Meeting.
        This URL will be used in the room list.

        :param external_id: Room model external_id
        :type external_id: string

        :return: URL
        :rtype: string
        """
        pass
