import re

import requests
from bigbluebutton_api_python.core import ApiMethod
from bigbluebutton_api_python.util import UrlBuilder
from django.conf import settings
from django.db import connections

from .BBBProviderInterface import BBBProviderInterface


class GreenlightProvider(BBBProviderInterface):
    gl_user_match_field = 'email'

    @classmethod
    def dictfetchall(cls, cursor):
        """Return all rows from a cursor as a dict"""
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

    @classmethod
    def _get_room_data(cls, gl_id):
        with connections['greenlight'].cursor() as cursor:
            cursor.execute('SELECT * FROM rooms WHERE deleted=FALSE AND uid=%s', [gl_id])
            return cls.dictfetchall(cursor)[0]

    @classmethod
    def get_meeting_running_url(cls, external_id):
        room_data = cls._get_room_data(external_id)
        urlbuilder = UrlBuilder(settings.BBB_API_ENDPOINT, settings.BBB_API_SECRET)
        return urlbuilder.buildUrl(ApiMethod.IS_MEETING_RUNNING, params={
            'meetingID': room_data['bbb_id'],
        })

    @classmethod
    def is_meeting_running(cls, external_id):
        url = cls.get_meeting_running_url(external_id)
        res = requests.get(url)
        if res.ok:
            m = re.search("<running>(true|false)</running>", res.text, flags=re.IGNORECASE)
            if m:
                return m.group(0).lower() == 'true'
        return False

    @classmethod
    def get_meeting_join_url(cls, external_id, additional_params=None):
        room_data = cls._get_room_data(external_id)
        params = {
            'meetingID': room_data['bbb_id'],
            'fullName': 'Live',
            'password': room_data['attendee_pw'],
            'userdata-bbb_auto_join_audio': True,
            'userdata-bbb_enable_video': True,
            'userdata-bbb_listen_only_mode': True,
            'userdata-bbb_force_listen_only': True,
            'userdata-bbb_skip_check_audio': True,
            'joinViaHtml5': True,
        }
        if additional_params is not None:
            params.update(additional_params)
        urlbuilder = UrlBuilder(settings.BBB_API_ENDPOINT, settings.BBB_API_SECRET)
        return urlbuilder.buildUrl(ApiMethod.JOIN, params=params)

    @classmethod
    def get_room_url(cls, external_id):
        return "{}/{}".format(settings.BBB_JOIN_URL, external_id)

    @classmethod
    def get_user_rooms(cls, user):
        username = cls._extract_match_field_from_user(user)
        all_rooms = cls._get_user_own_rooms(username)
        all_rooms.extend(cls._get_user_shared_rooms(username))
        return list({v['uid']: v for v in all_rooms}.values())  # unique list of dicts by uid

    @classmethod
    def _extract_match_field_from_user(cls, user):
        return user.username

    @classmethod
    def _get_user_own_rooms(cls, username):
        with connections['greenlight'].cursor() as cursor:
            cursor.execute('''SELECT rooms.uid, rooms.name FROM rooms
    JOIN users u on rooms.user_id = u.id
    WHERE rooms.deleted=FALSE AND u.{}=%s ORDER BY rooms.name'''.format(cls.gl_user_match_field), [username])
            return cls.dictfetchall(cursor)

    @classmethod
    def _get_user_shared_rooms(cls, username):
        with connections['greenlight'].cursor() as cursor:
            cursor.execute('''SELECT rooms.uid, rooms.name FROM rooms
    JOIN shared_accesses sa on rooms.id = sa.room_id
    JOIN users u on sa.user_id = u.id
    WHERE rooms.deleted=FALSE AND u.{}=%s ORDER BY rooms.name'''.format(cls.gl_user_match_field), [username])
            return cls.dictfetchall(cursor)

class GreenlightSocialProvider(GreenlightProvider):
    gl_user_match_field = 'social_uid'

    @classmethod
    def _extract_match_field_from_user(cls, user):
        return '{}@{}'.format(user.username, settings.GREENLIGHT_SOCAL_DOMAIN)

    #@classmethod
    #def _get_user_own_rooms(cls, user_social_id):
    #    with connections['greenlight'].cursor() as cursor:
    #        cursor.execute('''SELECT rooms.uid, rooms.name FROM rooms
    #JOIN users u on rooms.user_id = u.id
    #WHERE rooms.deleted=FALSE AND u.social_uid=%s ORDER BY rooms.name''', [user_social_id])
    #        return cls.dictfetchall(cursor)

    #@classmethod
    #def _get_user_shared_rooms(cls, user_social_id):
    #    with connections['greenlight'].cursor() as cursor:
    #        cursor.execute('''SELECT rooms.uid, rooms.name FROM rooms
    #JOIN shared_accesses sa on rooms.id = sa.room_id
    #JOIN users u on sa.user_id = u.id
    #WHERE rooms.deleted=FALSE AND u.social_uid=%s ORDER BY rooms.name''', [user_social_id])
    #        return cls.dictfetchall(cursor)

    #@classmethod
    #def get_user_rooms(cls, user):
    #    user_social_id = cls._get_user_social_id(user.username)
    #    all_rooms = cls._get_user_own_rooms(user_social_id)
    #    all_rooms.extend(cls._get_user_shared_rooms(user_social_id))
    #    return list({v['uid']: v for v in all_rooms}.values())  # unique list of dicts by uid

