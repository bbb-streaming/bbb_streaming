import json
import logging
import uuid
from datetime import timedelta

import pika
from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone

from bbb_streaming.models import Worker, Room, RoomExceptionLog

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Room and Worker Scheduler'

    def add_arguments(self, parser):
        pass

    def check_worker_state(self):
        for worker in Worker.objects.all():

            message_data = {
                "action": "schedule_worker_check",
                "worker": str(worker.id)
            }

            self.channel.basic_publish(exchange='', routing_key=self.queue, body=bytes(json.dumps(message_data), encoding='utf-8'))

    def check_room_actions(self):
        for room in Room.objects.filter(action__isnull=False):

            message_data = {
                "action": "schedule_room_check",
                "room": str(room.id)
            }

            self.channel.basic_publish(exchange='', routing_key=self.queue, body=bytes(json.dumps(message_data), encoding='utf-8'))

    def handle(self, *args, **options):
        self.queue = settings.RABBIT_RECEIVE_QUEUE

        self.connection = pika.BlockingConnection(pika.ConnectionParameters(**settings.RABBIT_CONNECTION))
        self.channel = self.connection.channel()

        if self.channel.queue_declare(queue=self.queue, passive=True):
            self.check_worker_state()
            self.check_room_actions()
        else:
            logger.warning('Controller Queue {} does not exist'.format(self.queue))
            logger.error('Could not dispatch messages to controller.')

        self.connection.close()
