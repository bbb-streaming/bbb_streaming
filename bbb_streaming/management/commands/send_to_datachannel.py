import logging
import socket


from django.core.management.base import BaseCommand

from bbb_streaming.models import StreamingEndpoint

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Send JSON to all Datachannels'

    def add_arguments(self, parser):
        parser.add_argument('data', type=str, help="Data as JSON string")

    def send_to_datachannel(self, ip_addr, port, data_bytes):
        addr_info = socket.getaddrinfo(host=ip_addr, port=port, type=socket.SOCK_DGRAM)
        sock = socket.socket(addr_info[0][0], addr_info[0][1])
        sock.sendto(data_bytes, (ip_addr, port))
        sock.close()

    def handle(self, *args, **options):

        data_bytes = bytes(options['data'], 'utf-8')

        for se in StreamingEndpoint.objects.all():
           self.send_to_datachannel(se.janus_server.rtp_ip, se.data_port, data_bytes)
