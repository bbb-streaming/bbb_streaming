import json
import logging
import socket


from django.core.management.base import BaseCommand

from bbb_streaming.models import Room

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Publish viewers to Datachannel'

    def add_arguments(self, parser):
        pass

    def send_to_datachannel(self, ip_addr, port, data):
        addr_info = socket.getaddrinfo(host=ip_addr, port=port, type=socket.SOCK_DGRAM)
        sock = socket.socket(addr_info[0][0], addr_info[0][1])
        sock.sendto(bytes(json.dumps(data), "utf-8"), (ip_addr, port))
        sock.close()

    def handle(self, *args, **options):

        prometheus_output = []

        for room in Room.objects.exclude(streaming_endpoints=None).prefetch_related('streaming_endpoints', 'streaming_endpoints__janus_server'):
            viewers = 0
            for se in room.streaming_endpoints.filter(created=True):
                try:
                    data = se.info()
                    se_viewers = data['plugindata']['data']['info']['viewers']
                    viewers += se_viewers
                    prometheus_output.append('janus_viewers{{server="{}",endpoint="{}",room="{}",room_ext="{}"}} {}'.format(se.janus_server.rest_endpoint, se.id, room.id, room.external_id, se_viewers))
                except Exception:
                    logger.exception("Failed to fetch viewer Stats")

            for se in room.streaming_endpoints.all():
                self.send_to_datachannel(se.janus_server.rtp_ip, se.data_port, {'viewers': viewers})

        if len(prometheus_output) > 0:
            print('# HELP Janus Stream Viewers')
            print('# TYPE janus_viewers')
            print('\n'.join(prometheus_output))

