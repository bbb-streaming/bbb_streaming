import json
import logging
import uuid
from datetime import timedelta

import pika
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand
from django.utils import timezone

from bbb_streaming.models import Worker, Room, RoomExceptionLog

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Message-Queue Worker'

    def add_arguments(self, parser):
        pass

    def process_message(self, body):
        try:
            data = json.loads(body)
        except json.JSONDecodeError:
            logger.exception("Unable to decode json")
            return

        action = data.get('action', None)

        if action is None:
            logger.warning("Received message without action")
            return

        if action == "schedule_room_action":
            room_id = data.get('room', None)

            if room_id is None:
                logger.warning("Received schedule_room_action but missing room")
                return

            try:
                room = Room.objects.get(id=uuid.UUID(room_id))
            except Room.DoesNotExist:
                logger.warning("Received schedule_room_action for unknown room")
            else:
                if room.action is not None:
                    try:
                        if room.action == Room.Action.START:
                            room.start_streaming()
                        elif room.action == Room.Action.STOP:
                            room.stop_streaming()
                        elif room.action == Room.Action.PAUSE:
                            room.pause_streaming()
                        elif room.action == Room.Action.UNPAUSE:
                            room.unpause_streaming()
                    except Exception as e:
                        logger.exception("Room {} action '{}' failed".format(room.id, room.get_action_display()))
                        RoomExceptionLog.objects.create(
                            log=str(e),
                            action=room.action,
                            room=room,
                        )
                else:
                    logger.warning("Received schedule_room_action but action is none")

        elif action == "schedule_room_check":
            room_id = data.get('room', None)

            if room_id is None:
                logger.warning("Received schedule_room_check but missing room")
                return

            try:
                room = Room.objects.get(id=uuid.UUID(room_id))
            except Room.DoesNotExist:
                logger.warning("Received schedule_room_check for unknown room")
            else:
                if room.action_time and room.action_time < (timezone.now() - timedelta(minutes=1)):
                    logger.error("Room {} action '{}' did not succeed within 1 minute".format(room.id, room.get_action_display()))
                    RoomExceptionLog.objects.create(
                        log="Room action timed out",
                        action=room.action,
                        room=room,
                    )
                    if room.action == Room.Action.START:
                        room.worker = None
                        room.save()
                    room.unset_action()

        elif action == "schedule_worker_check":
            worker_id = data.get('worker', None)

            if worker_id is None:
                logger.warning("Received schedule_worker_check but missing room")
                return

            try:
                worker = Worker.objects.get(id=uuid.UUID(worker_id))
            except Room.DoesNotExist:
                logger.warning("Received schedule_worker_check for unknown worker")
            else:
                if worker.ping and worker.ping < (timezone.now() - timedelta(minutes=2)):
                    if worker.state != Worker.State.NO_CONNECTION:
                        logger.critical("Worker {} connection lost".format(worker.id))
                        worker.state = Worker.State.NO_CONNECTION
                        worker.save()

                    if worker.state in [Worker.State.FAILED, Worker.State.NO_CONNECTION]:
                        # Reschedule streaming if worker failed
                        if worker.room is not None:
                            logger.info("Worker {} failed. Restarting room {}".format(worker.id, worker.room.id))
                            room = worker.room
                            worker.room = None
                            worker.save()
                            room.set_action(Room.Action.START)

        else:
            worker_id = data.get('worker', None)
            metadata = data.get('metadata', dict())
            nonce = data.get('nonce', "")

            if worker_id is None:
                logger.warning("Received {} without worker id".format(action))
                return

            try:
                worker = Worker.objects.get(id=uuid.UUID(worker_id))
            except Worker.DoesNotExist:
                logger.warning("Received {} from unregistered worker {}".format(action, str(worker_id)))
                if action == 'register':
                    worker = Worker.objects.create(id=uuid.UUID(worker_id), metadata=metadata, nonce=nonce, ping=timezone.now())
                    logger.info("Registered Worker {}".format(worker_id))
            else:
                if action == 'register':
                    logger.info("Worker {} already known".format(worker_id))
                    if worker.nonce != nonce:
                        worker.nonce = nonce
                        logger.info("Worker {} changed nonce".format(worker_id))
                        if worker.room:
                            logger.info("Assuming that worker {} restarted. Restarting room {}".format(worker_id, worker.room.id))
                            room = worker.room
                            worker.room = None
                            room.set_action(Room.Action.START)
                        worker.save()
                elif action == 'unregister':
                    worker.delete()
                    logger.info("Unregistered Worker {}".format(worker_id))
                elif action == 'ping':
                    worker.ping = timezone.now()
                    worker.save()
                elif action == 'set_state':
                    state_str = data.get('state', 'Unknown')
                    try:
                        state = Worker.State[state_str.upper()]
                    except:
                        logger.warning("Worker {} tried to set unknown state".format(worker.id))
                    else:
                        if worker.state == Worker.State.FAILED:
                            logger.info("Worker {} already failed. Not excepting new state {}".format(worker_id, state))
                            return
                        worker.state = state
                        worker.ping = timezone.now()
                        worker.save()
                        logger.info("Worker {} set to state {}".format(worker_id, worker.get_state_display()))
                        if state == Worker.State.FAILED:
                            logger.critical("Worker {} set itself failed".format(worker.id))

                        if worker.room:
                            if worker.room.action:
                                if worker.room.action == Room.Action.START and state == Worker.State.BBB_INACTIVE_PAUSED:
                                    logger.info("Worker {} successfully started streaming for room {}".format(worker.id, worker.room.id))
                                    worker.room.unset_action()
                                elif worker.room.action == Room.Action.PAUSE and state in [Worker.State.BBB_ACTIVE_PAUSED, Worker.State.BBB_INACTIVE_PAUSED]:
                                    logger.info("Worker {} successfully paused streaming for room {}".format(worker.id, worker.room.id))
                                    for se in worker.room.streaming_endpoints.all():
                                        try:
                                            se.stop_recording()
                                        except:
                                            pass
                                    worker.room.unset_action()
                                elif worker.room.action == Room.Action.UNPAUSE and state == Worker.State.BBB_ACTIVE_STREAMING:
                                    logger.info("Worker {} successfully unpaused streaming for room {}".format(worker.id, worker.room.id))
                                    for se in worker.room.streaming_endpoints.all():
                                        try:
                                            se.start_recording()
                                        except:
                                            pass
                                    worker.room.unset_action()
                                elif worker.room.action == Room.Action.STOP and state == Worker.State.IDLE:
                                    logger.info("Worker {} successfully stopped streaming for room {}".format(worker.id, worker.room.id))
                                    worker.room.unset_action()
                                    worker.room = None
                                    worker.save()
                        else:
                            logger.warning("Worker {} set state {} but has no room assigned".format(worker.id, state.name))

                else:
                    logger.warning("Received undefined action.")

    def handle(self, *args, **options):
        queue = settings.RABBIT_RECEIVE_QUEUE

        connection = pika.BlockingConnection(pika.ConnectionParameters(**settings.RABBIT_CONNECTION))
        channel = connection.channel()

        channel.queue_declare(queue=queue, durable=True)

        def callback(ch, method, properties, body):
            logger.debug("Received message: {}".format(str(body)))
            try:
                self.process_message(body)
            except:
                logger.exception('Unable to process message')
            ch.basic_ack(delivery_tag=method.delivery_tag)

        channel.basic_qos(prefetch_count=1)
        channel.basic_consume(queue=queue, on_message_callback=callback)

        channel.start_consuming()
