from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from bbb_streaming.models import StreamingEndpoint


@receiver(post_save, sender=StreamingEndpoint)
def save_endpoint_instance(sender, instance, created, **kwargs):
    if created:
        instance.set_media_ports()
