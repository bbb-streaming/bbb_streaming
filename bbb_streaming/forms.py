from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, ButtonHolder, Submit
from .models import Room

class RoomForm(ModelForm):
    class Meta:
        model = Room
        fields = ('pause_url',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Field(
                'pause_url'
            ),
            ButtonHolder(
                Submit('submit', 'Submit', css_class='button white')
            )
        )

