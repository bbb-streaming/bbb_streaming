import json
import logging
import random
import uuid
from datetime import timedelta

import pika
import requests
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import SET_NULL
from django.utils import timezone
from django.utils.module_loading import import_string
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)

bbb_provider = import_string(settings.BBB_PROVIDER)

def get_janus_defaults():
    return {
        'type': 'rtp',  # rtp|live|ondemand|rtsp
        'description': '',  # This is my awesome stream
        'metadata': '',
        # An optional string that can contain any metadata (e.g., JSON) associated with the stream you want users to receive
        'is_private': False,  # true|false (private streams don't appear when you do a 'list' request)
        # 'filename': '',  # path to the local file to stream (only for live/ondemand)
        'secret': str(uuid.uuid4()),
        # <optional password needed for manipulating (e.g., destroying or enabling/disabling) the stream>
        # 'pin': 0000,  # <optional password needed for watching the stream>
        'audio': True,  # true|false (do/don't stream audio)
        'video': True,  # true | false(do / don't stream video)
        'data': True,  # true|false (do/don't stream text via datachannels)
        # 'audioport': ports['audio'],  # local port for receiving audio frames
        # 'audiortcpport': ports['audiortcp'],  # local port for receiving and sending audio RTCP feedback
        # 'audiomcast': '',  # multicast group for receiving audio frames, if any
        # 'audioiface': '',  # network interface or IP address to bind to, if any (binds to all otherwise)
        'audiopt': 111,  # <audio RTP payload type> (e.g., 111)
        'audiortpmap': 'opus/48000/2',  # RTP map of the audio codec (e.g., opus/48000/2)
        # 'audiofmtp': '',  # Codec specific parameters, if any
        # 'audioskew': False,  # true|false (whether the plugin should perform skew analisys and compensation on incoming audio RTP stream, EXPERIMENTAL)
        # 'videoport': ports['video'],  # local port for receiving video frames (only for rtp)
        # 'videortcpport': ports['videortcp'],  # local port for receiving and sending video RTCP feedback
        # 'videomcast': '',  # multicast group for receiving video frames, if any
        # 'videoiface': '',  # network interface or IP address to bind to, if any (binds to all otherwise)
        'videopt': 100,  # <video RTP  payload type> (e.g., 100)
        'videortpmap': 'VP8/90000',  # RTP map of the video codec (e.g., VP8/90000)
        'videofmtp': '',  # Codec specific parameters, if any
        'videobufferkf': True,
        # true|false (whether the plugin should store the latest keyframe and send it immediately for new viewers, EXPERIMENTAL)
        'videosimulcast': True,  # true|false (do|don't enable video simulcasting)
        # 'videoport2': ports['video2'],
        # second local port for receiving video frames (only for rtp, and simulcasting)
        # 'videoport3': ports['video3'],
        # third local port for receiving video frames (only for rtp, and simulcasting)
        # 'videoskew': False,  # true|false (whether the plugin should perform skew analisys and compensation on incoming video RTP stream, EXPERIMENTAL)
        # 'videosvc': False,  # true|false (whether the video will have SVC support; works only for VP9-SVC, default=false)
        'collision': 60 * 1000,
        # in case of collision (more than one SSRC hitting the same port), the plugin will discard incoming RTP packets with a new SSRC unless this many milliseconds passed, which would then change the current SSRC (0=disabled)
        # 'dataport': ports['data'],  # local port for receiving data messages to relay
        # 'dataiface': '',  # network interface or IP address to bind to, if any (binds to all otherwise)
        # 'datatype': 'text',  # text|binary (type of data this mountpoint will relay, default=text)
        # 'databuffermsg': False,  # true|false (whether the plugin should store the latest message and send it immediately for new viewers)
        # 'threads': 8,
        # number of threads to assist with the relaying part, which can help if you expect a lot of viewers that may cause the RTP receiving part in the Streaming plugin to slow down and fail to catch up (default=0)
        # In case you want to use SRTP for your RTP-based mountpoint, you'll need
        # to configure the SRTP-related properties as well, namely the suite to
        # use for hashing (32 or 80) and the crypto information for decrypting
        # the stream (as a base64 encoded string the way SDES does it). Notice
        # that with SRTP involved you'll have to pay extra attention to what you
        # feed the mountpoint, as you may risk getting SRTP decrypt errors:
        # 'srtpsuite': 32,
        # 'srtpcrypto': 'WbTBosdVUZqEb6Htqhn+m3z7wUh4RJVR8nE15GbN'
        #
        # The Streaming plugin can also be used to (re)stream media that has been
        # encrypted using something that can be consumed via Insertable Streams.
        # In that case, we only need to be aware of it, so that we can send the
        # info along with the SDP. How to decrypt the media is out of scope, and
        # up to the application since, again, this is end-to-end encryption and
        # so neither Janus nor the Streaming plugin have access to anything.
        # DO NOT SET THIS PROPERTY IF YOU DON'T KNOW WHAT YOU'RE DOING!
        # 'e2ee': True
        #
        # The following options are only valid for the 'rstp' type:
        # 'url': '',   # RTSP stream URL
        # 'rtsp_user': '',  # RTSP authorization username, if needed
        # 'rtsp_pwd': '', # RTSP authorization password, if needed
        # 'rtsp_failcheck': '',  # whether an error should be returned if connecting to the RTSP server fails (default=true)
        # 'rtspiface': '',  # network interface IP address or device name to listen on when receiving RTSP streams
    }


def get_unused_startport():
    allowed_ports = set(range(settings.JANUS_MEDIA_PORT_START, settings.JANUS_MEDIA_PORT_START+2000, 10))
    used_ports = set(StreamingEndpoint.objects.all().values_list('startport', flat=True))
    available_ports = allowed_ports - used_ports
    return min(available_ports)


def validate_startport(value):
    if value < settings.JANUS_MEDIA_PORT_START:
        raise ValidationError(
            _('%(value)s is below defined start port.'),
            params={'value': value},
        )

    if (value % settings.JANUS_MEDIA_PORT_STEP) != 0:
        raise ValidationError(
            _('%(value)s is not a valid step size.'),
            params={'value': value},
        )


class JanusAPIException(requests.RequestException):
    def __init__(self, *args, **kwargs):
        super(JanusAPIException, self).__init__(*args, **kwargs)


class JanusStreamingServer(models.Model):
    rest_endpoint = models.CharField(max_length=256, unique=True, blank=False)
    ws_endpoint = models.CharField(max_length=256, unique=True, blank=True)
    rtp_ip = models.GenericIPAddressField(blank=False)
    admin_key = models.CharField(max_length=256, blank=False)
    enabled = models.BooleanField(default=True)

    class Meta:
        verbose_name = _('Streaming Server')
        verbose_name_plural = _('Streaming Server')

    def __str__(self):
        return self.rest_endpoint

    def check_error(self, response):
        response.raise_for_status()

        try:
            data = response.json()
        except Exception as e:
            raise JanusAPIException(
                "ERROR parsing Janus-Gateway response",
                response=response,
            )
        else:
            logger.debug("Janus response data: {}".format(json.dumps(data)))
            error_code = ''
            error_message = ''
            if 'error' in data:
                error_code = data['error']['code']
                error_message = data['error']['reason']
            elif 'plugindata' in data and 'error' in data['plugindata']['data']:
                error_code = data['plugindata']['data']['error_code']
                error_message = data['plugindata']['data']['error']

            if error_code or error_message:
                raise JanusAPIException(
                    "ERROR calling Janus-Gateway: {} ({})".format(error_message, error_code),
                    response=response,
                )

            return data

    def call_janus(self, body):
        transaction = str(uuid.uuid4())
        base_url = self.rest_endpoint.strip('/')

        # Get Session
        resp = requests.post(base_url, json={
            "janus": "create",
            "transaction": transaction,
        })
        resp_data = self.check_error(resp)
        session_id = resp_data['data']['id']

        # Get Handle
        url = "{}/{}".format(base_url, session_id)
        resp = requests.post(url, json={
            "janus": "attach",
            "plugin": "janus.plugin.streaming",
            "transaction": transaction,
        })
        resp_data = self.check_error(resp)
        handle = resp_data['data']['id']

        # Plugin Request
        url = "{}/{}/{}".format(base_url, session_id, handle)
        post_data = {
            "janus": "message",
            "transaction": transaction,
            "body": body,
        }
        logger.debug("Janus post data: {}".format(json.dumps(post_data)))
        resp = requests.post(url, json=post_data)
        return_data = self.check_error(resp)

        # Detach Handle
        url = "{}/{}/{}".format(base_url, session_id, handle)
        resp = requests.post(url, json={
            "janus": "detach",
            "transaction": transaction,
        })
        self.check_error(resp)

        # Destroy Session
        url = "{}/{}".format(base_url, session_id)
        resp = requests.post(url, json={
            "janus": "destroy",
            "transaction": transaction,
        })
        self.check_error(resp)

        return return_data

    def call_stream_endpoint(self, body):
        """
        Calls janus streaming plugin

        :param body: dict
        :return: bool|dict
        """

        data = {
            "admin_key": self.admin_key
        }
        data.update(body)
        return self.call_janus(data)

    def list_stream_endpoint(self):
        body = {
            "request": "list"
        }

        return self.call_stream_endpoint(body)


class StreamingEndpoint(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    endpoint_id = models.UUIDField(default=uuid.uuid4, editable=False)
    janus_data = models.JSONField(default=get_janus_defaults)
    created = models.BooleanField(default=False)
    enabled = models.BooleanField(default=True)
    recording = models.BooleanField(default=False)
    startport = models.PositiveIntegerField(default=get_unused_startport, unique=True, validators=[validate_startport])
    janus_server = models.ForeignKey(JanusStreamingServer, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = _('Streaming Endpoint')
        verbose_name_plural = _('Streaming Endpoints')

    def __str__(self):
        return str(self.id)

    @property
    def audio_port(self):
        return self.startport + 1

    @property
    def video_port_lq(self):
        return self.startport + 3

    @property
    def video_port_mq(self):
        return self.startport + 5

    @property
    def video_port_hq(self):
        return self.startport + 7

    @property
    def data_port(self):
        return self.startport + 9

    def set_media_ports(self):
        data = {
            'audioport': self.audio_port,  # XXX1
            'audiortcpport': self.startport + 2,  # XXX2
            'videoport': self.video_port_lq,  # XXX3
            'videortcpport': self.startport + 4,  # XXX4
            'videoport2': self.video_port_mq,  # XXX5
            'videoport3': self.video_port_hq,  # XXX7
            'dataport': self.data_port,  # XXX9
        }
        data.update(self.janus_data)
        self.janus_data = data
        self.save()

    def info(self):
        """
        Get info for endpoint from Janus-Gateway

        :return: dict
        """

        if not self.janus_server:
            raise RuntimeError("No Janus server defined for endpoint {}".format(self.id))

        data = {
            'request': 'info',
            'id': str(self.endpoint_id),
            'secret': self.janus_data['secret'],
        }

        try:
            resp_data = self.janus_server.call_stream_endpoint(data)
        except JanusAPIException as e:
            logger.exception("Failed to fetch StreamingEndpoint Info")
            StreamingEndpointExceptionLog.objects.create(
                log=str(e),
                response=e.response.text,
                action=StreamingEndpointExceptionLog.Action.INFO,
                endpoint=self,
            )
            raise
        else:
            return resp_data

    def deploy(self):
        """
        Create Endpoint on Janus-Gateway

        :return:
        """

        if not self.janus_server:
            raise RuntimeError("No Janus server defined for endpoint {}".format(self.id))

        data = {
            'request': 'create',
            'id': str(self.endpoint_id),
        }
        data.update(self.janus_data)
        try:
            self.janus_server.call_stream_endpoint(data)
        except JanusAPIException as e:
            logger.exception("Failed to create StreamingEndpoint")
            StreamingEndpointExceptionLog.objects.create(
                log=str(e),
                response=e.response.text,
                action=StreamingEndpointExceptionLog.Action.CREATE,
                endpoint=self,
            )
            if "A stream with the provided ID already exists" in e.response.text:
                self.created = True
                self.save()
            else:
                raise
        else:
            self.created = True
            self.save()

    def edit(self):
        """
        Update update Endpoint on Janus-Gateway

        :return:
        """

        if not self.janus_server:
            raise RuntimeError("No Janus server defined for endpoint {}".format(self.id))

        data = {
            'request': 'edit',
            'id': str(self.endpoint_id),
            'secret': self.janus_data['secret'],
        }

        try:
            resp_data = self.info()
            actual_data = resp_data['plugindata']['data']['info']
            for key, value in self.janus_data.items():
                if key in actual_data:
                    if actual_data[key] != value:
                        data['new_{}'.format(key)] = value
                else:
                    data['new_{}'.format(key)] = value

            self.janus_server.call_stream_endpoint(data)
        except JanusAPIException as e:
            logger.exception("Failed to edit StreamingEndpoint")
            StreamingEndpointExceptionLog.objects.create(
                log=str(e),
                response=e.response.text,
                action=StreamingEndpointExceptionLog.Action.EDIT,
                endpoint=self,
            )
            raise
        finally:
            self.created = False
            self.save()

    def destroy(self):
        """
        Delete Endpoint on Janus-Gateway

        :return:
        """

        if not self.janus_server:
            raise RuntimeError("No Janus server defined for endpoint {}".format(self.id))

        if self.recording:
            self.stop_recording()

        data = {
            'request': 'destroy',
            'id': str(self.endpoint_id),
            'secret': self.janus_data['secret'],
        }
        try:
            self.janus_server.call_stream_endpoint(data)
        except JanusAPIException as e:
            logger.exception("Failed to destroy StreamingEndpoint")
            StreamingEndpointExceptionLog.objects.create(
                log=str(e),
                response=e.response.text,
                action=StreamingEndpointExceptionLog.Action.DESTROY,
                endpoint=self,
            )
            if "No such mountpoint" not in e.response.text:
                raise

    def enable(self):
        """
        Enable Endpoint on Janus-Gateway

        :return:
        """

        if not self.janus_server:
            raise RuntimeError("No Janus server defined for endpoint {}".format(self.id))

        data = {
            'request': 'enable',
            'id': str(self.endpoint_id),
            'secret': self.janus_data['secret'],
        }
        try:
            self.janus_server.call_stream_endpoint(data)
        except JanusAPIException as e:
            logger.exception("Failed to enable StreamingEndpoint")
            StreamingEndpointExceptionLog.objects.create(
                log=str(e),
                response=e.response.text,
                action=StreamingEndpointExceptionLog.Action.ENABLE,
                endpoint=self,
            )
            raise
        else:
            self.enabled = True
            self.save()

    def disable(self):
        """
        Disable Endpoint on Janus-Gateway

        :return:
        """

        if not self.janus_server:
            raise RuntimeError("No Janus server defined for endpoint {}".format(self.id))

        if self.recording:
            self.stop_recording()

        data = {
            'request': 'disable',
            'id': str(self.endpoint_id),
            'secret': self.janus_data['secret'],
        }
        try:
            self.janus_server.call_stream_endpoint(data)
        except JanusAPIException as e:
            logger.exception("Failed to disable StreamingEndpoint")
            StreamingEndpointExceptionLog.objects.create(
                log=str(e),
                response=e.response.text,
                action=StreamingEndpointExceptionLog.Action.DISABLE,
                endpoint=self,
            )
            raise
        else:
            self.enabled = False
            self.save()

    def start_recording(self):
        """
        Start Recording on Janus-Gateway

        :return:
        """

        if not self.janus_server:
            raise RuntimeError("No Janus server defined for endpoint {}".format(self.id))

        if self.recording:
            logger.info("Request to start recording stream {} but stream already recoding.".format(self.id))
            return

        filename_base = '{}/{}_{}'.format(getattr(settings, 'JANUS_RECORDING_DIR', '/recordings'), str(self.endpoint_id), timezone.now().isoformat())
        data = {
            'request': 'recording',
            'action': 'start',
            'id': str(self.endpoint_id),
            'secret': self.janus_data['secret'],
        }
        if self.janus_data['audio']:
            data.update({
                'audio': '{}_audio'.format(filename_base),
            })
        if self.janus_data['video']:
            data.update({
                'video': '{}_video'.format(filename_base),
            })
        if self.janus_data['data']:
            data.update({
                'data': '{}_data'.format(filename_base),
            })

        try:
            self.janus_server.call_stream_endpoint(data)
        except JanusAPIException as e:
            logger.exception("Failed to start recording StreamingEndpoint")
            StreamingEndpointExceptionLog.objects.create(
                log=str(e),
                response=e.response.text,
                action=StreamingEndpointExceptionLog.Action.START_RECORDING,
                endpoint=self,
            )
            raise
        else:
            self.recording = True
            self.save()

    def stop_recording(self):
        """
        Stop Recording on Janus-Gateway

        :return:
        """

        if not self.janus_server:
            raise RuntimeError("No Janus server defined for endpoint {}".format(self.id))

        if not self.recording:
            logger.info("Request to stop recording stream {} but stream not recoding.".format(self.id))
            return

        data = {
            'request': 'recording',
            'action': 'stop',
            'id': str(self.endpoint_id),
            'secret': self.janus_data['secret'],
            'audio': self.janus_data['audio'],
            'video': self.janus_data['video'],
            'data': self.janus_data['data'],
        }

        try:
            self.janus_server.call_stream_endpoint(data)
        except JanusAPIException as e:
            logger.exception("Failed to stop recording StreamingEndpoint")
            StreamingEndpointExceptionLog.objects.create(
                log=str(e),
                response=e.response.text,
                action=StreamingEndpointExceptionLog.Action.STOP_RECORDING,
                endpoint=self,
            )
            raise
        else:
            self.recording = False
            self.save()


class ExceptionLog(models.Model):
    class Meta:
        abstract = True
        ordering = ['time']

    id = models.BigAutoField(primary_key=True)
    time = models.DateTimeField(auto_now_add=True, editable=False)
    log = models.TextField(blank=True, editable=False)

    @classmethod
    def clean_logs(cls, days=7):
        cls.objects.filter(time__lte=timedelta(days=days)).delete()


class StreamingEndpointExceptionLog(ExceptionLog):
    class Action(models.IntegerChoices):
        CREATE = 0, 'Create'
        EDIT = 1, 'Edit'
        DESTROY = 2, 'Destroy'
        ENABLE = 3, 'Enable'
        DISABLE = 4, 'Disable'
        START_RECORDING = 5, 'Start recording'
        STOP_RECORDING = 6, 'Stop recording'
        INFO = 7, 'Info'
        LIST = 8, 'List'

        __empty__ = 'Unknown'

    action = models.IntegerField(choices=Action.choices, null=True, editable=False)
    response = models.TextField(blank=True, editable=False)
    endpoint = models.ForeignKey(StreamingEndpoint, null=True, on_delete=models.SET_NULL)


class Room(models.Model):
    class Action(models.IntegerChoices):
        START = 1, 'Start streaming'
        STOP = 2, 'Stop Streaming'
        PAUSE = 3, 'Pause streaming'
        UNPAUSE = 4, 'Unpause streaming'

        __empty__ = 'Nothing'

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    external_id = models.CharField(max_length=256, blank=False, unique=True)
    streaming_endpoints = models.ManyToManyField(to=StreamingEndpoint, blank=True)
    action = models.IntegerField(choices=Action.choices, null=True, blank=True)
    action_time = models.DateTimeField(null=True, blank=True, default=None)
    pause_url = models.URLField(max_length=255, blank=True, default='', 
                                help_text=_("The content of this URL will be downloaded by the worker and played in a loop. It must be a video. Recommended format is 1080p30, 48 kHz audio"),
                               )

    class Meta:
        verbose_name = _('Room')
        verbose_name_plural = _('Rooms')

    def __str__(self):
        return self.external_id

    def set_action(self, a):
        if self.action != a:
            self.action = a
            self.action_time = timezone.now()
            self.save()

            queue = settings.RABBIT_RECEIVE_QUEUE

            message_data = {
                "action": "schedule_room_action",
                "room": str(self.id),
            }

            connection = pika.BlockingConnection(pika.ConnectionParameters(**settings.RABBIT_CONNECTION))
            channel = connection.channel()

            if channel.queue_declare(queue=queue, passive=True):
                channel.basic_publish(exchange='', routing_key=queue,
                                      body=bytes(json.dumps(message_data), encoding='utf-8'))
            else:
                logger.warning('Controller Queue {} does not exist'.format(queue))
                raise Exception('Could not dispatch message to controller.')

            connection.close()

    def unset_action(self):
        self.action = None
        self.action_time = None
        self.save()

    def start_streaming(self):
        """
        Creates StreamingEndpoint and dispatches message for workers with new streaming job

        :return:
        """
        if Worker.objects.filter(state=Worker.State.IDLE, room__isnull=True).count() == 0:
            raise RuntimeError("No Worker in idle state. No Worker may be available.")

        not_configured_janus_servers = JanusStreamingServer.objects\
            .filter(enabled=True)\
            .exclude(id__in=self.streaming_endpoints.all().values_list('janus_server__id', flat=True))

        for js in not_configured_janus_servers:
            se = StreamingEndpoint.objects.create(endpoint_id=self.id, janus_server=js)
            self.streaming_endpoints.add(se)

        streaming_targets = []
        for se in self.streaming_endpoints.all():
            if not se.created:
                se.deploy()
            if not se.enabled:
                se.enable()
            streaming_targets.append({
                'janus_host': se.janus_server.rtp_ip,
                'audio_port': se.audio_port,
                'video_port_hq': se.video_port_hq,
                'video_port_mq': se.video_port_mq,
                'video_port_lq': se.video_port_lq,
            })

        worker_data = {
            'room': self.external_id,
            'action': 'start_streaming',
            'meeting_running_url': bbb_provider.get_meeting_running_url(self.external_id),
            'meeting_join_url': bbb_provider.get_meeting_join_url(self.external_id),
            'pause_url': self.pause_url,
            'streaming_targets': streaming_targets
        }

        try:
            self.worker
        except Room.worker.RelatedObjectDoesNotExist:
            worker = random.choice(list(Worker.objects.filter(state=Worker.State.IDLE, room__isnull=True)))
            worker.room = self
            worker.save()
        else:
            worker = self.worker
        worker.send_message(worker_data)

    def stop_streaming(self):
        """
        Dispatches 'stop_streaming' message to worker and deletes StreamingEndpoint

        :return:
        """
        worker = self.worker
        worker.send_message({
            'room': self.external_id,
            'action': 'stop_streaming'
        })

        for se in self.streaming_endpoints.all():
            try:
                se.disable()
            except JanusAPIException:
                pass

    def get_viewers(self):
        viewers = 0
        for se in self.streaming_endpoints.all():
            data = se.info()
            viewers += data['plugindata']['data']['info']['viewers']
        return viewers

    def is_meeting_running(self):
        """
        Check if meeting is running

        :return: bool
        """
        return bbb_provider.is_meeting_running(self.external_id)

    def pause_streaming(self):
        self.worker.send_message({
            'room': self.external_id,
            'action': 'pause_streaming'
        })

    def unpause_streaming(self):
        self.worker.send_message({
            'room': self.external_id,
            'action': 'unpause_streaming'
        })

    def toggle_chat(self):
        """
        Dispatches message to worker to toggle chat window and toggles chat view for streaming client

        :return:
        """
        pass

    def send_chat_message(self, message):
        """
        Dispatches message to worker with chat message

        :return:
        """
        pass

    def toggle_notes(self):
        """
        Dispatches message to worker to toggle notes window

        :return:
        """
        pass

    def toggle_presentation(self):
        """
        Dispatches message to worker to toggle presentation

        :return:
        """
        pass


class RoomExceptionLog(ExceptionLog):
    action = models.IntegerField(choices=Room.Action.choices, null=True, editable=False)
    room = models.ForeignKey(Room, null=True, on_delete=models.SET_NULL)


class Worker(models.Model):
    class State(models.IntegerChoices):
        FAILED = 1, 'Failed'
        IDLE = 2, 'Idle'
        BBB_INACTIVE_STREAMING = 3, _('Streaming (Room inactive)')
        BBB_ACTIVE_STREAMING = 4, _('Streaming')
        BBB_INACTIVE_PAUSED = 5, _('Paused (Room inactive)')
        BBB_ACTIVE_PAUSED = 6, _('Paused')
        NO_CONNECTION = 7, _('Lost Connection')

        __empty__ = _('Unknown')

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    state = models.IntegerField(choices=State.choices, null=True, blank=True)
    room = models.OneToOneField(to=Room, null=True, blank=True, on_delete=SET_NULL)
    metadata = models.JSONField(default=dict, blank=True)
    nonce = models.CharField(max_length=64, blank=True, default="")
    ping = models.DateTimeField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = _('Worker')
        verbose_name_plural = _('Workers')

    def __str__(self):
        return str(self.id)

    def send_message(self, message_data=None):
        """
        Dispatches message to worker including `message_data` as json

        :param message_data: dict
        :return:
        """
        if message_data is None:
            message_data = {}
        message_data['nonce'] = self.nonce
        connection = pika.BlockingConnection(pika.ConnectionParameters(**settings.RABBIT_CONNECTION))
        channel = connection.channel()

        if channel.queue_declare(queue=str(self.id), passive=True):
            channel.basic_publish(exchange='', routing_key=str(self.id), body=bytes(json.dumps(message_data), encoding='utf-8'))
        else:
            logger.warning('Worker Queue {} does not exist'.format(str(self.id)))
            raise Exception('Could not dispatch message to worker.')

        connection.close()
