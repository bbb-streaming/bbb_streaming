from unittest import mock

from django.test import TestCase

from bbb_streaming.management.commands.mq_worker import Command as MqWorkerCommand
from bbb_streaming.management.commands.scheduler import Command as SchedulerCommand
# Create your tests here.
from bbb_streaming.models import *
from bbb_streaming.tests._janus_test_mocks import mocked_janus_post_fabric, JANUS_STREAMING_MESSAGE_ERROR_RESPONSE


class RoomTestCase(TestCase):
    databases = {'default', 'greenlight'}

    def setUp(self):
        self.test_worker = Worker.objects.create(id=uuid.UUID('bc5f4f70-c463-4b4d-8f8b-f0dbc68c0628'), nonce='B6iGENB6iz', state=Worker.State.IDLE)

    @mock.patch('bbb_streaming.models.pika.BlockingConnection', spec=pika.BlockingConnection)
    def test_room_start(self, mocked_connection):
        mock_resp = mocked_janus_post_fabric()
        with mock.patch('bbb_streaming._janus_helper.requests.post', mock_resp) as m:
            gl_id = 'joh-ij0-kdn-xt8'
            r = Room.objects.create(gl_id=gl_id)
            r.start_streaming()

            m.assert_called()
            mocked_connection.return_value.channel.return_value.queue_declare.assert_called()
            mocked_connection.return_value.channel.return_value.basic_publish.assert_called_with(
                exchange='',
                routing_key=str(self.test_worker.id),
                body=bytes(json.dumps({
                    'room': r.gl_id,
                    'action': 'start_streaming',
                    'meeting_running_url': get_meeting_running_url(gl_id),
                    'meeting_join_url': get_meeting_join_url(gl_id),
                    'janus_host': settings.JANUS_MEDIA_IP,
                    'audio_port': r.streaming_endpoint.audio_port,
                    'video_port_hq': r.streaming_endpoint.video_port_hq,
                    'video_port_mq': r.streaming_endpoint.video_port_mq,
                    'video_port_lq': r.streaming_endpoint.video_port_lq,
                    'nonce': self.test_worker.nonce,
                }), encoding='utf8')
            )
            mocked_connection.return_value.close.assert_called()
            w = Worker.objects.get(id=self.test_worker.id)
            self.assertEqual(r, w.room)

    @mock.patch('bbb_streaming.models.pika.BlockingConnection', spec=pika.BlockingConnection)
    def test_room_stop(self, mocked_connection):
        mock_resp = mocked_janus_post_fabric()
        with mock.patch('bbb_streaming._janus_helper.requests.post', mock_resp) as m:
            gl_id = 'joh-ij0-kdn-xt8'
            r = Room.objects.create(gl_id=gl_id)
            r.start_streaming()

            m.assert_called()
            mocked_connection.return_value.channel.return_value.queue_declare.assert_called()
            mocked_connection.return_value.channel.return_value.basic_publish.assert_called_with(
                exchange='',
                routing_key=str(self.test_worker.id),
                body=bytes(json.dumps({
                    'room': r.gl_id,
                    'action': 'start_streaming',
                    'meeting_running_url': get_meeting_running_url(gl_id),
                    'meeting_join_url': get_meeting_join_url(gl_id),
                    'janus_host': settings.JANUS_MEDIA_IP,
                    'audio_port': r.streaming_endpoint.audio_port,
                    'video_port_hq': r.streaming_endpoint.video_port_hq,
                    'video_port_mq': r.streaming_endpoint.video_port_mq,
                    'video_port_lq': r.streaming_endpoint.video_port_lq,
                    'nonce': self.test_worker.nonce,
                }), encoding='utf8')
            )
            mocked_connection.return_value.close.assert_called()
            w = Worker.objects.get(id=self.test_worker.id)
            self.assertEqual(r, w.room)

            r = Room.objects.get(gl_id=gl_id)
            r.stop_streaming()
            m.assert_called()
            mocked_connection.return_value.channel.return_value.queue_declare.assert_called()
            mocked_connection.return_value.channel.return_value.basic_publish.assert_called_with(
                exchange='',
                routing_key=str(self.test_worker.id),
                body=bytes(json.dumps({
                    'room': r.gl_id,
                    'action': 'stop_streaming',
                    'nonce': self.test_worker.nonce,
                }), encoding='utf8')
            )
            mocked_connection.return_value.close.assert_called()

    @mock.patch('bbb_streaming.models.pika.BlockingConnection', spec=pika.BlockingConnection)
    def test_room_failed(self, mocked_connection):
        mock_resp = mocked_janus_post_fabric()
        with mock.patch('bbb_streaming._janus_helper.requests.post', mock_resp) as m:
            gl_id = 'joh-ij0-kdn-xt8'
            r = Room.objects.create(gl_id=gl_id)
            r.start_streaming()

            m.assert_called()
            mocked_connection.return_value.channel.return_value.queue_declare.assert_called()
            mocked_connection.return_value.channel.return_value.basic_publish.assert_called_with(
                exchange='',
                routing_key=str(self.test_worker.id),
                body=bytes(json.dumps({
                    'room': r.gl_id,
                    'action': 'start_streaming',
                    'meeting_running_url': get_meeting_running_url(gl_id),
                    'meeting_join_url': get_meeting_join_url(gl_id),
                    'janus_host': settings.JANUS_MEDIA_IP,
                    'audio_port': r.streaming_endpoint.audio_port,
                    'video_port_hq': r.streaming_endpoint.video_port_hq,
                    'video_port_mq': r.streaming_endpoint.video_port_mq,
                    'video_port_lq': r.streaming_endpoint.video_port_lq,
                    'nonce': self.test_worker.nonce,
                }), encoding='utf8')
            )
            mocked_connection.return_value.close.assert_called()

            mocked_connection.reset_mock()
            mocked_connection.return_value.channel.return_value.queue_declare.return_value = False
            with self.assertRaises(Exception):
                r.stop_streaming()
            m.assert_called()
            mocked_connection.return_value.channel.return_value.basic_publish.assert_not_called()
            mocked_connection.return_value.close.assert_not_called()


class WorkerTestCase(TestCase):
    def setUp(self):
        pass

    def test_management_command_mq_worker_process_message(self):
        c = MqWorkerCommand()

        c.process_message('{')  # JSON decode error
        c.process_message('{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9"}')  # no action
        c.process_message('{"action": "register"}')  # no worker
        c.process_message('{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "foo"}')  # worker unregistered

        c.process_message('{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "register", "nonce": "foobar", "metadata": {"host": "batz.example.com"}}')
        worker = Worker.objects.get(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))
        worker.room = Room.objects.create(gl_id='adm-2dx-byr-cec')
        worker.save()

        c.process_message('{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "register", "nonce": "batz", "metadata": {"host": "batz.example.com"}}')
        worker = Worker.objects.get(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))
        self.assertEqual(worker.nonce, 'batz')
        self.assertEqual(worker.room.action, Room.Action.START)

        c.process_message('{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "foo"}')  # undefined action
        c.process_message('{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "set_state", "state": "foo"}')  # undefined state

        for name, member in Worker.State.__members__.items():
            d = {"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "set_state", "state": name.lower()}
            c.process_message(json.dumps(d))
            worker = Worker.objects.get(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))
            self.assertEqual(worker.state, member)

        c.process_message('{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "set_state", "state": "idle"}')
        worker = Worker.objects.get(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))
        worker.room.set_action(Room.Action.START)
        c.process_message('{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "set_state", "state": "bbb_inactive_paused"}')
        worker = Worker.objects.get(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))
        self.assertIsNone(worker.room.action)

        worker.room.set_action(Room.Action.PAUSE)
        c.process_message(
            '{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "set_state", "state": "bbb_active_paused"}')
        worker = Worker.objects.get(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))
        self.assertIsNone(worker.room.action)

        worker.room.set_action(Room.Action.PAUSE)
        c.process_message(
            '{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "set_state", "state": "bbb_inactive_paused"}')
        worker = Worker.objects.get(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))
        self.assertIsNone(worker.room.action)

        worker.room.set_action(Room.Action.UNPAUSE)
        c.process_message(
            '{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "set_state", "state": "bbb_active_streaming"}')
        worker = Worker.objects.get(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))
        self.assertIsNone(worker.room.action)

        worker.room.set_action(Room.Action.STOP)
        c.process_message(
            '{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "set_state", "state": "idle"}')
        worker = Worker.objects.get(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))
        self.assertIsNone(worker.room)

        worker.room = None
        worker.save()
        c.process_message(
            '{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "set_state", "state": "idle"}')

        c.process_message('{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "ping"}')

        c.process_message('{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "unregister"}')
        with self.assertRaises(Worker.DoesNotExist):
            worker = Worker.objects.get(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))

    @mock.patch('bbb_streaming.models.pika.BlockingConnection', spec=pika.BlockingConnection)
    def test_dispatch_message_to_worker(self, mocked_connection):
        test_dict = {
            'test': 'test'
        }
        w = Worker.objects.create()
        w.send_message()
        mocked_connection.return_value.channel.return_value.queue_declare.assert_called()
        mocked_connection.return_value.channel.return_value.basic_publish.assert_called()
        mocked_connection.return_value.close.assert_called()

    @mock.patch('bbb_streaming.models.pika.BlockingConnection', spec=pika.BlockingConnection)
    def test_dispatch_message_to_worker_failed(self, mocked_connection):
        mocked_connection.return_value.channel.return_value.queue_declare.return_value = False
        w = Worker.objects.create()
        with self.assertRaises(Exception):
            w.send_message()
        mocked_connection.return_value.channel.return_value.queue_declare.assert_called()
        mocked_connection.return_value.channel.return_value.basic_publish.assert_not_called()
        mocked_connection.return_value.close.assert_not_called()

        mocked_connection.return_value.channel.return_value.queue_declare.side_effect = ValueError
        w = Worker.objects.create()
        with self.assertRaises(ValueError):
            w.send_message()
        mocked_connection.return_value.channel.return_value.queue_declare.assert_called()
        mocked_connection.return_value.channel.return_value.basic_publish.assert_not_called()
        mocked_connection.return_value.close.assert_not_called()


class SchedulerTestCase(TestCase):
    databases = {'default', 'greenlight'}

    def test_management_command_scheduler_check_worker_state(self):
        c = SchedulerCommand()
        worker = Worker.objects.create(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))
        worker.ping = (timezone.now() - timedelta(minutes=6))
        worker.save()
        c.check_worker_state()

        worker = Worker.objects.get(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))
        self.assertEqual(worker.state, Worker.State.NO_CONNECTION)

        worker.state = Worker.State.IDLE
        worker.room = Room.objects.create(gl_id='adm-2dx-byr-cec')
        worker.save()

        c.check_worker_state()
        worker = Worker.objects.get(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))
        self.assertEqual(worker.state, Worker.State.NO_CONNECTION)
        self.assertIsNone(worker.room)

        room = Room.objects.get(gl_id='adm-2dx-byr-cec')
        self.assertEqual(room.action, Room.Action.START)

    def test_management_command_scheduler_process_room_actions(self):
        c = SchedulerCommand()

        room = Room.objects.create(gl_id='adm-2dx-byr-cec')

        room.set_action(Room.Action.START)
        with mock.patch('bbb_streaming.management.commands.scheduler.Room.start_streaming') as m:
            c.process_room_actions()
            m.assert_called()

        room.set_action(Room.Action.STOP)
        with mock.patch('bbb_streaming.management.commands.scheduler.Room.stop_streaming') as m:
            c.process_room_actions()
            m.assert_called()

        room.set_action(Room.Action.PAUSE)
        with mock.patch('bbb_streaming.management.commands.scheduler.Room.pause_streaming') as m:
            c.process_room_actions()
            m.assert_called()

        room.set_action(Room.Action.UNPAUSE)
        with mock.patch('bbb_streaming.management.commands.scheduler.Room.unpause_streaming') as m:
            c.process_room_actions()
            m.assert_called()

        room.set_action(Room.Action.START)
        room.action_time = (timezone.now() - timedelta(minutes=6))
        room.save()
        with mock.patch('bbb_streaming.management.commands.scheduler.Room.start_streaming') as m:
            c.process_room_actions()
            m.assert_called()
            rel = RoomExceptionLog.objects.first()
            self.assertEqual(rel.room.id, room.id)

        room.set_action(Room.Action.START)
        room.action_time = (timezone.now() - timedelta(minutes=2))
        room.save()
        with mock.patch('bbb_streaming.management.commands.scheduler.Room.start_streaming') as m:
            c.process_room_actions()
            m.assert_called()
            room = Room.objects.get(gl_id='adm-2dx-byr-cec')
            with self.assertRaises(Room.worker.RelatedObjectDoesNotExist):
                _worker = room.worker


class StreamingEndpointTestCase(TestCase):

    def test_create_streaming_endpoints(self):
        mock_resp = mocked_janus_post_fabric()
        with mock.patch('bbb_streaming._janus_helper.requests.post', mock_resp) as m:
            ep1 = StreamingEndpoint.objects.create()
            self.assertEqual(ep1.janus_data['audioport'], 5001)
            self.assertEqual(ep1.janus_data['audiortcpport'], 5002)
            self.assertEqual(ep1.janus_data['videoport'], 5003)
            self.assertEqual(ep1.janus_data['videortcpport'], 5004)
            self.assertEqual(ep1.janus_data['videoport2'], 5005)
            self.assertEqual(ep1.janus_data['videoport3'], 5007)
            self.assertEqual(ep1.janus_data['dataport'], 5009)

            ep2 = StreamingEndpoint.objects.create()
            self.assertEqual(ep2.janus_data['audiortcpport'], 5012)
            self.assertEqual(ep2.janus_data['videoport'], 5013)
            self.assertEqual(ep2.janus_data['videortcpport'], 5014)
            self.assertEqual(ep2.janus_data['videoport2'], 5015)
            self.assertEqual(ep2.janus_data['videoport3'], 5017)
            self.assertEqual(ep2.janus_data['dataport'], 5019)

            ep3 = StreamingEndpoint.objects.create()
            self.assertEqual(ep3.janus_data['audioport'], 5021)
            ep4 = StreamingEndpoint.objects.create()
            self.assertEqual(ep4.janus_data['audioport'], 5031)
            ep5 = StreamingEndpoint.objects.create()
            self.assertEqual(ep5.janus_data['audioport'], 5041)

            ep2.delete()

            ep6 = StreamingEndpoint.objects.create()
            self.assertEqual(ep6.janus_data['audiortcpport'], 5052)

            ep6.delete()
            ep3.delete()

            ep7 = StreamingEndpoint.objects.create()
            self.assertEqual(ep7.janus_data['audioport'], 5061)


    def test_streaming_endpoint_create(self):
        mock_resp = mocked_janus_post_fabric()
        with mock.patch('bbb_streaming._janus_helper.requests.post', mock_resp) as m:
            ep1 = StreamingEndpoint.objects.create()
            ep1.deploy()
            m.assert_called()

    def test_streaming_endpoint_create_failed(self):
        mock_resp = mocked_janus_post_fabric(
            json_response=JANUS_STREAMING_MESSAGE_ERROR_RESPONSE
        )
        with mock.patch('bbb_streaming._janus_helper.requests.post', mock_resp) as m:
            ep1 = StreamingEndpoint.objects.create()
            with self.assertRaises(JanusAPI.JanusAPIException):
                ep1.deploy()
            m.assert_called()
            self.assertFalse(StreamingEndpoint.objects.get(id=ep1.id).created)


    def test_streaming_endpoint_delete(self):
        mock_resp = mocked_janus_post_fabric()
        with mock.patch('bbb_streaming._janus_helper.requests.post', mock_resp) as m:
            ep1 = StreamingEndpoint.objects.create()
            ep1.deploy()
            m.assert_called()
            ep1.delete()
            ep1.destroy()

    def test_streaming_endpoint_delete_failed(self):

        mock_resp = mocked_janus_post_fabric()
        with mock.patch('bbb_streaming._janus_helper.requests.post', mock_resp) as m:
            ep1 = StreamingEndpoint.objects.create()
            ep1.deploy()
            m.assert_called()

        mock_resp = mocked_janus_post_fabric(
            json_response=JANUS_STREAMING_MESSAGE_ERROR_RESPONSE
        )
        with mock.patch('bbb_streaming._janus_helper.requests.post', mock_resp) as m2:
            with self.assertRaises(JanusAPI.JanusAPIException):
                ep1.delete()
                ep1.destroy()
                m2.assert_called()


class IntegrationTestCase(TestCase):
    databases = {'default', 'greenlight'}

    @mock.patch('bbb_streaming.models.pika.BlockingConnection', spec=pika.BlockingConnection)
    def test_room_and_worker_together(self, mocked_connection):
        s = SchedulerCommand()
        mq = MqWorkerCommand()

        mock_resp = mocked_janus_post_fabric()
        with mock.patch('bbb_streaming._janus_helper.requests.post', mock_resp) as m:

            room = Room.objects.create(gl_id='joh-ij0-kdn-xt8')
            Worker.objects.create(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'), state=Worker.State.IDLE)

            # start
            room.set_action(Room.Action.START)
            s.process_room_actions()
            mocked_connection.return_value.channel.return_value.queue_declare.assert_called()
            mocked_connection.return_value.channel.return_value.basic_publish.assert_called()
            m.assert_called()

            mocked_connection.reset_mock()
            m.reset_mock()
            s.process_room_actions()
            mocked_connection.return_value.channel.return_value.queue_declare.assert_not_called()
            mocked_connection.return_value.channel.return_value.basic_publish.assert_not_called()

            mq.process_message('{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "set_state", "state": "bbb_inactive_paused"}')
            m.assert_not_called()

            worker = Worker.objects.get(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))
            room = Room.objects.get(gl_id='joh-ij0-kdn-xt8')
            self.assertEqual(worker.room, room)
            self.assertEqual(worker.state, Worker.State.BBB_INACTIVE_PAUSED)
            self.assertIsNone(room.action)

            # unpause
            m.reset_mock()
            mq.process_message('{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "set_state", "state": "bbb_active_paused"}')
            worker = Worker.objects.get(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))
            self.assertEqual(worker.state, Worker.State.BBB_ACTIVE_PAUSED)

            room.set_action(Room.Action.UNPAUSE)
            s.process_room_actions()
            mq.process_message('{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "set_state", "state": "bbb_active_streaming"}')

            worker = Worker.objects.get(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))
            room = Room.objects.get(gl_id='joh-ij0-kdn-xt8')
            self.assertEqual(worker.state, Worker.State.BBB_ACTIVE_STREAMING)
            self.assertIsNone(room.action)

            # pause
            m.reset_mock()
            room.set_action(Room.Action.PAUSE)
            s.process_room_actions()
            mq.process_message('{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "set_state", "state": "bbb_active_paused"}')

            worker = Worker.objects.get(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))
            room = Room.objects.get(gl_id='joh-ij0-kdn-xt8')
            self.assertEqual(worker.state, Worker.State.BBB_ACTIVE_PAUSED)
            self.assertIsNone(room.action)

            # stop
            m.reset_mock()

            room.set_action(Room.Action.STOP)
            s.process_room_actions()
            mq.process_message('{"worker": "f10da7d7-8ac9-4959-8f26-5c0c6c5399f9", "action": "set_state", "state": "idle"}')
            m.assert_called()

            worker = Worker.objects.get(id=uuid.UUID('f10da7d7-8ac9-4959-8f26-5c0c6c5399f9'))
            room = Room.objects.get(gl_id='joh-ij0-kdn-xt8')
            with self.assertRaises(Room.worker.RelatedObjectDoesNotExist):
                _worker = room.worker
            self.assertEqual(worker.state, Worker.State.IDLE)
            self.assertIsNone(room.action)