import json
from unittest import mock

JANUS_CREATE_RESPONSE = {
    "data": {
        "id": 7211760465655344
    },
    "janus": "success",
    "transaction": "f858d590-aba8-4c6b-a50f-4afc29558844"
}

JANUS_DESTROY_RESPONSE = {
    "janus": "success",
    "session_id": 7211760465655344,
    "transaction": "f858d590-aba8-4c6b-a50f-4afc29558844"
}

JANUS_ERROR_RESPONSE = {
    "error": {
        "code": 458,
        "reason": "No such session 7211760465655344"
    },
    "janus": "error",
    "session_id": 7211760465655344,
    "transaction": "f858d590-aba8-4c6b-a50f-4afc29558844"
}

JANUS_ATTACH_STEAMING_RESPONSE = {
    "data": {
        "id": 716616424383476
    },
    "janus": "success",
    "session_id": 7211760465655344,
    "transaction": "f858d590-aba8-4c6b-a50f-4afc29558844"
}

JANUS_DETACH_RESPONSE = {
    "janus": "success",
    "session_id": 7211760465655344,
    "transaction": "f858d590-aba8-4c6b-a50f-4afc29558844"
}

JANUS_STREAMING_MESSAGE_DUMMY_RESPONSE = {
    "janus": "success",
    "plugindata": {
        "data": {
            "streaming": "ok"
        },
        "plugin": "janus.plugin.streaming"
    },
    "sender": 716616424383476,
    "session_id": 7211760465655344,
    "transaction": "f858d590-aba8-4c6b-a50f-4afc29558844"
}

JANUS_STREAMING_MESSAGE_INFO_RESPONSE = {
    "janus": "success",
    "plugindata": {
        "data": {
            "info": {
                "audio": True,
                "audio_age_ms": 84508,
                "audiopt": 111,
                "audiortpmap": "opus/48000/2",
                "description": "Opus/VP8 live stream coming from external source",
                "enabled": True,
                "id": "1",
                "metadata": "You can use this metadata section to put any info you want!",
                "type": "live",
                "video": True,
                "video_age_ms": 84508,
                "videopt": 100,
                "videortpmap": "VP8/90000",
                "videosimulcast": True
            },
            "streaming": "info"
        },
        "plugin": "janus.plugin.streaming"
    },
    "sender": 716616424383476,
    "session_id": 7211760465655344,
    "transaction": "f858d590-aba8-4c6b-a50f-4afc29558844"
}

JANUS_STREAMING_MESSAGE_CREATE_RESPONSE = {
    "janus": "success",
    "plugindata": {
        "data": {
            "streaming": "created",
            "create": "",
            "permanent": False,
            "stream": {}
        },
        "plugin": "janus.plugin.streaming"
    },
    "sender": 716616424383476,
    "session_id": 7211760465655344,
    "transaction": "f858d590-aba8-4c6b-a50f-4afc29558844"
}

JANUS_STREAMING_MESSAGE_EDIT_RESPONSE = {
    "janus": "success",
    "plugindata": {
        "data": {
            "streaming": "edited",
            "id": 0,
            "permanent": False,
        },
        "plugin": "janus.plugin.streaming"
    },
    "sender": 716616424383476,
    "session_id": 7211760465655344,
    "transaction": "f858d590-aba8-4c6b-a50f-4afc29558844"
}

JANUS_STREAMING_MESSAGE_DESTROY_RESPONSE = {
    "janus": "success",
    "plugindata": {
        "data": {
            "streaming": "destroyed",
            "id": 0,
        },
        "plugin": "janus.plugin.streaming"
    },
    "sender": 716616424383476,
    "session_id": 7211760465655344,
    "transaction": "f858d590-aba8-4c6b-a50f-4afc29558844"
}

JANUS_STREAMING_MESSAGE_ERROR_RESPONSE = {
    "janus": "success",
    "plugindata": {
        "data": {
            "error": "Missing mandatory element (secret)",
            "error_code": 453,
            "streaming": "event"
        },
        "plugin": "janus.plugin.streaming"
    },
    "sender": 716616424383476,
    "session_id": 7211760465655344,
    "transaction": "f858d590-aba8-4c6b-a50f-4afc29558844"
}


def mocked_janus_post_fabric(*args, **kwargs):
    raise_for_status = kwargs.pop('raise_for_status', None)
    status = kwargs.pop('status', 200)
    json_response = kwargs.pop('json_response', None)

    def mocked_janus_post(*args2, **kwargs2):
        json_request = kwargs2.pop('json', None)

        mock_resp = mock.Mock()
        mock_resp.raise_for_status = mock.Mock()
        if raise_for_status:
            mock_resp.raise_for_status.side_effect = raise_for_status
        mock_resp.status_code = status

        json_data = {}

        if json_response is not None:
            json_data = json_response
        else:
            if json_request is not None and 'janus' in json_request:
                if json_request['janus'] == 'create':
                    json_data = JANUS_CREATE_RESPONSE
                elif json_request['janus'] == 'attach':
                    if json_request['plugin'] == 'janus.plugin.streaming':
                        json_data = JANUS_ATTACH_STEAMING_RESPONSE
                elif json_request['janus'] == 'detach':
                    json_data = JANUS_DETACH_RESPONSE
                elif json_request['janus'] == 'destroy':
                    json_data = JANUS_DESTROY_RESPONSE
                elif json_request['janus'] == 'message':
                    if json_request['body']['request'] == 'info':
                        json_data = JANUS_STREAMING_MESSAGE_INFO_RESPONSE
                    elif json_request['body']['request'] == 'create':
                        json_data = JANUS_STREAMING_MESSAGE_CREATE_RESPONSE
                    elif json_request['body']['request'] == 'destroy':
                        json_data = JANUS_STREAMING_MESSAGE_DESTROY_RESPONSE
                    elif json_request['body']['request'] == 'edit':
                        json_data = JANUS_STREAMING_MESSAGE_EDIT_RESPONSE
                    else:
                        json_data = JANUS_STREAMING_MESSAGE_DUMMY_RESPONSE

        mock_resp.json = mock.Mock(
            return_value=json_data
        )
        mock_resp.text = json.dumps(json_data)

        return mock_resp

    return mock.Mock(side_effect=mocked_janus_post)
