import requests
from django.conf import settings
from django.test import TestCase
from requests import HTTPError

from bbb_streaming._janus_helper import JanusAPI
from bbb_streaming.tests._janus_test_mocks import *


class JanusHelperTestCase(TestCase):

    def setUp(self):
        self.janus_session_id = '7211760465655344'
        self.janus_handle_id = '716616424383476'
        self.janus_mock_base_url = settings.JANUS_API_ENDPOINT
        self.janus_mock_session_url = '{}/{}'.format(self.janus_mock_base_url, self.janus_session_id)
        self.janus_mock_handle_url = '{}/{}/{}'.format(self.janus_mock_base_url, self.janus_session_id,
                                                       self.janus_handle_id)

    def test_janus_helper_call_stream_endpoint(self):
        mock_resp = mocked_janus_post_fabric()
        with mock.patch('bbb_streaming._janus_helper.requests.post', mock_resp) as m:
            rest_data = JanusAPI.call_stream_endpoint({
                "request": "info",
                "id": "1",
                'secret': '33d3c91f-59fe-4a32-a95f-41dd88fbc398',
            })

            self.assertDictEqual(rest_data, JANUS_STREAMING_MESSAGE_INFO_RESPONSE)

    def test_janus_helper_call_stream_endpoint_fail(self):
        mock_resp = mocked_janus_post_fabric(
            status=500,
            raise_for_status=HTTPError("500 Server Error"),
            json_response=JANUS_ERROR_RESPONSE
        )
        with mock.patch('bbb_streaming._janus_helper.requests.post', mock_resp) as m:
            with self.assertRaises(requests.RequestException):
                rest_data = JanusAPI.call_stream_endpoint({
                    "request": "info",
                    "id": "1",
                    'secret': '33d3c91f-59fe-4a32-a95f-41dd88fbc398',
                })

    def test_janus_helper_call_stream_endpoint_error(self):
        mock_resp = mocked_janus_post_fabric(
            json_response=JANUS_ERROR_RESPONSE
        )
        with mock.patch('bbb_streaming._janus_helper.requests.post', mock_resp) as m:
            with self.assertRaises(JanusAPI.JanusAPIException):
                rest_data = JanusAPI.call_stream_endpoint({
                    "request": "info",
                    "id": "1",
                    'secret': '33d3c91f-59fe-4a32-a95f-41dd88fbc398',
                })

        mock_resp = mocked_janus_post_fabric(
            json_response=JANUS_STREAMING_MESSAGE_ERROR_RESPONSE
        )
        with mock.patch('bbb_streaming._janus_helper.requests.post', mock_resp) as m:
            with self.assertRaises(JanusAPI.JanusAPIException):
                rest_data = JanusAPI.call_stream_endpoint({
                    "request": "info",
                    "id": "1",
                    'secret': '33d3c91f-59fe-4a32-a95f-41dd88fbc398',
                })
