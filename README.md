Streaming BigBlueButton Conferences using WebRTC
================================================

This package is a Django app that you can use to build a low latency live streaming 
application for BigBlueButton conferences. The following components are involved:

* a set of worker nodes. Each of the runs an instance of bbb-rtp-streamer. 
  Get it at https://gitlab.com/bbb-streaming/bbb-rtp-streamer
  The workers will launch a chromium instance which joins BBB. Chrome and audio are 
  captured using a GStreamer Pipeline. The result is streamed using RTP packets to 
  a janus instance. Each worker node can capture exactly one BBB conference at a 
  time.
* A server running the janus software. Janus receives RTP Streams from the worker 
  nodes and distributes them as WebRTC streams. All WebRTC handling is done in 
  Janus. Janus' streaming plugin is used to distribute the streams. Currently only 
  one Janus instance is supported, but it should not be too hard to add support 
  for more servers to scale horizontaly. 
* This sofware is the controller part. It sends messages to the workers to instruct 
  them on which conference to capture and where to send the RTP packets to. It also
  provides the user interface for the streams. The controller configures Janus and 
  creates one streaming plugin instance for each conference.
* A RabbitMQ instance. RabbitMQ is used to exchange messages between workers and 
  controller


Including this into your application
------------------------------------

* add the project to you requirements.txt
  ~~~
  -e git+ssh://gitlab.com:bbb-streaming/bbb_streaming.git@master#egg=bbb_streaming
  ~~~
* add it to the INSTALLED_APPS setting
  ~~~python
  INSTALLED_APPS = [                                                              
    ...
    'bbb_streaming',
  ]
  ~~~
* add it to the project urls.py
  ~~~python
  from django.contrib import admin
  from django.urls import path, include

  urlpatterns = [
      path('admin/', admin.site.urls),
      path('', include('bbb_streaming.urls')),
  ]
  ~~~
* configure the following settings:
  * `BBB_PROVIDER`: a class which provides BBB related data like user rooms and join urls (https://gitlab.com/bbb-streaming/bbb_streaming/-/blob/master/bbb_streaming/bbb_provider/BBBProviderInterface.py)
  * Greenlight-Provider related:
    * `BBB_PROVIDER`: `bbb_streaming.bbb_provider.GreenlightProvider`
    * `DATABASES`: add a (readonly-)connection named `greenlight`
    * `GREENLIGHT_SOCAL_DOMAIN`: The name of the social domain used in Greenlight for you users (will search for users: `username@<GREENLIGHT_SOCAL_DOMAIN>`)
    * `BBB_JOIN_URL`: Greenlight Base URL. Will be used to create a link to join a BBB room like `https://example.com/gl/foo-bar-baz` (`<BBB_JOIN_URL>/external_room_id`)
    * BigBlueButton related:
      * `BBB_API_ENDPOINT`: The API endpoint of your BBB Server or Scalelite instance
      * `BBB_API_SECRET`: The API secret for BBB
  * `RABBIT_CONNECTION`: Add the connection parameters required for RabbitMQ. Example:
    ~~~python
    RABBIT_CONNECTION = {
        'host': 'rabbitmq.example.com',
        'virtual_host': '/bbb_streaming',
        'credentials': pika.PlainCredentials('bbb_streaming', 'change_me'),
    }
    ~~~
  * Janus:
    * `JANUS_LB_REST_URL`: URL to the Janus Rest API LoadBalancer
    * `JANUS_LB_WSS_URL`: URL to the Janus WebSocket LoadBalancer
    * `JANUS_MEDIA_PORT_START`: The UDP port from which janus will start accepting connections
    * `JANUS_MEDIA_PORT_STEP`: how many UDP ports will be reserved for each stream. Use `10`. There are 4 streams: 1 audio stream and 3 video streams, one per resolution. Each connection requires 2 UDP ports, one for RTP packets and one for RTCP. Port allocation on the janus server is controlled by this streaming controller.
    * `TURN_SECRET`: the shared secret for your TURN Server
    * `TURN_URI`: example: "turns:turnserver.example.com:443?transport=tcp"
    * `STUN_URI`: example: "stun:turnserver.example.com"
  * Theming (optional):
    * `THEME_BASE_WATCH`: Name of the base template used for the watch view
    * `THEME_BASE_LIST`: Name of the base template used for the list view
* setup a RabbitMQ Server
* setup a Janus server
* deploy some workers

